package com.twilsontech.x.repositories.jpa;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.auth.AuthorizationContext;

public interface AuthorizationContextRepository extends
		PagingAndSortingRepository<AuthorizationContext, Integer> {

	List<AuthorizationContext> findByPrincipalInAndResourceTypeAndAction(Set<String> principals, String resourceType, DomainAction domainAction, Sort sort);
	
	List<AuthorizationContext> findByPrincipalInAndResourceTypeAndActionAndResourceIdIn(Set<String> principals, String resourceType, DomainAction domainAction, Collection<String> resourceIds, Sort sort);
	
}
