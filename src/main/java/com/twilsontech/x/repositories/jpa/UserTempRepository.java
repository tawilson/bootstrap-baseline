package com.twilsontech.x.repositories.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.UserTemp;

public interface UserTempRepository extends
		PagingAndSortingRepository<UserTemp, Integer> {
	
	public UserTemp findByUserIdAndKey(Integer userId, String key);

}
