package com.twilsontech.x.repositories.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.Media;

public interface MediaRepository extends
		PagingAndSortingRepository<Media, Integer> {
	
	Media findByOriginalFilename(String originalFilename);

}
