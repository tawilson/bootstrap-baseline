package com.twilsontech.x.repositories.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.ContentPage;

public interface ContentPageRepository extends PagingAndSortingRepository<ContentPage, Integer> {
	
	ContentPage findByUrlPath(String urlPath);

}
