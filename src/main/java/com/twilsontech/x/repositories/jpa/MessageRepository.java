package com.twilsontech.x.repositories.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.Message;

public interface MessageRepository extends
		PagingAndSortingRepository<Message, Integer> {
	
	List<Message> findByCreatedBetween(Date from, Date to);
	
	List<Message> findByMessageContaining(String query);

}
