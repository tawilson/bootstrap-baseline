package com.twilsontech.x.repositories.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.ContentPageContext;

public interface ContentPageContextRepository extends
		PagingAndSortingRepository<ContentPageContext, Integer> {

}
