package com.twilsontech.x.repositories.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.ApplicationProperty;

public interface ApplicationPropertiesRepository extends
		PagingAndSortingRepository<ApplicationProperty, Integer> {
	
	ApplicationProperty findByKey(String key);

}
