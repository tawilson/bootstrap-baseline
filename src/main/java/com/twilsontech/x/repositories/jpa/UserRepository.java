package com.twilsontech.x.repositories.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.User;

public interface UserRepository extends
		PagingAndSortingRepository<User, Integer> {

	User findByUsername(String username);
	
	Page<User> findByDeletedAndUsernameNot(boolean deleted, String username, Pageable pageable);

}
