package com.twilsontech.x.repositories.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.audit.AuditRecord;

public interface AuditRecordRepository extends PagingAndSortingRepository<AuditRecord, Integer>  {
	
	public AuditRecord findByResourceTypeAndResourceIdAndAction(ResourceType resourceType, String resourceId, DomainAction action);

}
