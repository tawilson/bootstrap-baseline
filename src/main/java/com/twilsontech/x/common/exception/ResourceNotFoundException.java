package com.twilsontech.x.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Will result in a 404 exception
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends DefaultException {

	private static final long serialVersionUID = 1L;

}
