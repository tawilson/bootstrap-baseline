package com.twilsontech.x.controller.api;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/api/v1/", produces = "application/json")
public class ApiController {
	
	@RequestMapping(value = "/ping")
	public @ResponseBody Map<String, String> ping() {
		HashMap<String, String> value = new HashMap<>();
		value.put("message", "I'm alive, it is " + new Date());
		return value;
	}

}
