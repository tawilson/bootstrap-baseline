package com.twilsontech.x.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * TODO: Document why
 *
 */
@Controller
public class ExceptionController {
	
	@RequestMapping("/errorDefault")
	public String defaultExceptionPage() {
		return "errors/default";
	}

}
