package com.twilsontech.x.controller.common;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


public class CustomPageRequest extends PageRequest {

	private static final long serialVersionUID = 1L;

	public CustomPageRequest (Pageable pageable) {
		// Mainly used by display tag views or paging through results with a 1 based index.  Converts to 0 based index.
		super((pageable.getPageNumber() == 0 ? 0 : pageable.getPageNumber() - 1), pageable.getPageSize(), pageable.getSort());
	}

}
