package com.twilsontech.x.controller.common;

public enum FlashMessageType {
	
	SUCCESS("alert-success"), INFO("alert-info"), WARNING("alert-warning"), DANGER("alert-danger");
	
	private FlashMessageType(String cssClass) {
		this.cssClass = cssClass;
	}

	private String cssClass;

	public String getCssClass() {
		return cssClass;
	}
	
}
