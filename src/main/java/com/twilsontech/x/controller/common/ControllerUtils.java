package com.twilsontech.x.controller.common;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ControllerUtils {
	
	private String pattern = "EEE, dd MMM yyyy HH:mm:ss 'GMT'";
	private SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
	
	private MessageSource messageSource;
	private AuthenticationManager authenticationManager;
	
	public void addGenericFlashMessage(RedirectAttributes redirectAttributes, FlashMessageType flashMessageType, String messageParams[]) {
		addFlashMessage(redirectAttributes, flashMessageType, "genericFlashMessage", messageParams);
	}
	
	public void addFlashMessage(RedirectAttributes redirectAttributes, FlashMessageType flashMessageType, String messageKey, String messageParams[]) {
		redirectAttributes.addFlashAttribute("flashCss", flashMessageType.getCssClass());
		redirectAttributes.addFlashAttribute("flashMessage", messageSource.getMessage(messageKey, messageParams, Locale.getDefault()));
	}
	
	public void addModelMessage(Model model, FlashMessageType flashMessageType, String messageKey, String messageParams[]) {
		model.addAttribute("flashCss", flashMessageType.getCssClass());
		model.addAttribute("flashMessage", messageSource.getMessage(messageKey, messageParams, Locale.getDefault()));
	}	
	
	public void addSessionMessage(HttpSession session, FlashMessageType flashMessageType, String messageKey, String messageParams[]) {
		session.setAttribute("flashCss", flashMessageType.getCssClass());
		session.setAttribute("flashMessage", messageSource.getMessage(messageKey, messageParams, Locale.getDefault()));
		session.setAttribute("sessionFlashMessage", true);
	}	
	
	public UserDetails getUserDetails(Principal principal) {
		return (UserDetails)((Authentication) principal).getPrincipal();
	}
	
	/**
	 * 
	 * @param username
	 * @param password - Unencrypted password
	 */
	public void authenticateUser(String username, String password) {
		UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(
				username, password);
		Authentication auth = authenticationManager.authenticate(upat);
		SecurityContext ctx = SecurityContextHolder.getContext();
		ctx.setAuthentication(auth);
	}		
	
	/** 
	 * Sets Last-Modified and Cache-Control headers.  Cache-Control is set to a year.
	 * @param response
	 * @param lastUpdate
	 */
	public void putCacheHeaders(HttpServletResponse response, Date lastUpdate) {		
		response.setHeader("Cache-Control", "public, max-age=" + 31556926);  // cache for a year
		response.setHeader("Last-Modified", format.format(lastUpdate));		
	}

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Autowired
	public void setAuthenticationManager(
			AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}	

}
