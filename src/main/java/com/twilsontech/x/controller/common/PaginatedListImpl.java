package com.twilsontech.x.controller.common;

import java.util.List;

import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;
import org.springframework.data.domain.Page;

public class PaginatedListImpl implements PaginatedList {

	private List<?> list;
	private int pageNumber;
	private int objectsPerPage;
	private int fullListSize;
	private String sortCriterion;
	private SortOrderEnum sortDirection;
	private String searchId;
	
	public PaginatedListImpl(Page<?> results) {
		this.list = results.getContent();
		this.pageNumber = results.getNumber() + 1;  // account for display tag index offset.
		this.objectsPerPage = results.getNumberOfElements();
		this.fullListSize = (int) results.getTotalElements();
		this.objectsPerPage = results.getSize();
	}

	@Override
	public List<?> getList() {
		return list;
	}

	@Override
	public int getPageNumber() {
		return pageNumber;
	}

	@Override
	public int getObjectsPerPage() {
		return objectsPerPage;
	}

	@Override
	public int getFullListSize() {
		return fullListSize;
	}

	@Override
	public String getSortCriterion() {
		return sortCriterion;
	}

	@Override
	public SortOrderEnum getSortDirection() {
		return sortDirection;
	}

	@Override
	public String getSearchId() {
		return searchId;
	}

}
