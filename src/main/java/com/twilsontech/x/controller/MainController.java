package com.twilsontech.x.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.twilsontech.x.service.ContentPageContextService;
import com.twilsontech.x.service.UserService;

@Controller
public class MainController {
	
	private UserService userService;
	private ContentPageContextService contentPageContextService; 
	
	@RequestMapping("/")
	public String homeIndex(Model model) {
		
		if(userService.isAuthenticated()) {
			return "redirect:/auth/home";
		} else {
			return prepareIndex(model);
		}
	
	}
	
	/**
	 * Used for direct links to the index page.
	 */
	@RequestMapping("/index")
	public String index(Model model) {
		return prepareIndex(model);
	}	
	
	@RequestMapping("/about")
	public String about() {
		return "about";
	}
	
	@RequestMapping("/contact")
	public String contact() {
		return "contact";
	}
	
	@RequestMapping("/help")
	public String help() {
		return "help";
	}
	
	@RequestMapping("/terms")
	public String terms() {
		return "terms";
	}
	
	@RequestMapping("/privacy")
	public String privacy() {
		return "privacy";
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setContentPageContextService(
			ContentPageContextService contentPageContextService) {
		this.contentPageContextService = contentPageContextService;
	}	
	
	private String prepareIndex(Model model) {
		model.addAttribute("content", contentPageContextService.findOne().getIndexPage().getContent());
		return "index";
	}	
	
}
