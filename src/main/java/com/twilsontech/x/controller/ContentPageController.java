package com.twilsontech.x.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.twilsontech.x.common.exception.ResourceNotFoundException;
import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.model.ContentPage;
import com.twilsontech.x.service.ContentPageService;

@RequestMapping("/p/")
@Controller
public class ContentPageController {
	
	private static final Logger logger = LoggerFactory.getLogger(ContentPageController.class);
	
	public ContentPageService contentPageService;
	private ControllerUtils controllerUtils;
	
	@RequestMapping(value = "{path}")
	public String showContent(@PathVariable String path, Model model, HttpServletResponse response) {
		
		ContentPage contentPage = contentPageService.findByUrlPath(path);
		
		if(contentPage == null) {
			
			throw new ResourceNotFoundException();
			
		} else {
			
			model.addAttribute("contentPage", contentPage);
			controllerUtils.putCacheHeaders(response, contentPage.getLastUpdate());
			
		}
		
		return "contentPage";
	}

	@Autowired
	public void setContentPageService(ContentPageService contentPageService) {
		this.contentPageService = contentPageService;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}
	
}
