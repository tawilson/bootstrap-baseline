package com.twilsontech.x.controller.auth.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.CustomPageRequest;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.controller.common.PaginatedListImpl;
import com.twilsontech.x.model.ContentPage;
import com.twilsontech.x.model.ContentPageContext;
import com.twilsontech.x.model.validator.ContentPageValidator;
import com.twilsontech.x.service.ContentPageContextService;
import com.twilsontech.x.service.ContentPageService;

@Controller
@RequestMapping("/auth/admin/pages")
@SessionAttributes("contentPage")
public class ContentPageManagerController {
	
	private ContentPageContextService contentPageContextService;
	private ContentPageService contentPageService;
	private ContentPageValidator contentPageValidator;
	private ControllerUtils controllerUtils;
	
	@RequestMapping(value = { "" })
	public String findAllPages(
			Model model,
			@PageableDefault(page = 0, size = 20, sort = { "pageTitle" }, direction = Direction.ASC) Pageable pageable) {
		
		Page<ContentPage> pages = contentPageService.findAll(new CustomPageRequest(pageable));
		model.addAttribute("pageList", new PaginatedListImpl(pages));
		return "auth/admin/pages/list";
	}
	
	@RequestMapping(value = "/new")
	public String newPage(Model model) {
		ContentPage contentPage = new ContentPage();
		return prepareModel(contentPage, model);
	}
	
	@RequestMapping(value = "/{id}")
	public String viewPage(@PathVariable Integer id, Model model) {
		ContentPage contentPage = contentPageService.findOne(new ContentPage(id));
		return prepareModel(contentPage, model);
	}	
	

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String savePage(@ModelAttribute ContentPage contentPage, BindingResult bindingResult, Model model, SessionStatus sessionStatus, RedirectAttributes redirectAttributes) {
		
		String view = null;
		
		contentPageValidator.validate(contentPage, bindingResult);
		if(bindingResult.hasErrors()) {
			controllerUtils.addModelMessage(model, FlashMessageType.DANGER, "genericInvalidSubmission", new String[]{"page"});
			view = prepareModel(contentPage, model);
		} else {
			if(contentPage.isNew()) {
				contentPageService.create(contentPage);
			} else {
				contentPageService.update(contentPage);
			}
			controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"page","saved"});
			sessionStatus.setComplete();
			view = "redirect:/auth/admin/pages/" + contentPage.getId();
		}
		return view;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deletePage(@PathVariable Integer id, Model model, SessionStatus sessionStatus, RedirectAttributes redirectAttributes) {
		
		String view = null;
		
		ContentPageContext contentPageContext = contentPageContextService.findOne();
		if(contentPageContext.getIndexPage().getId().equals(id)) {
			// you cannot delete a page if its the index.
			controllerUtils.addFlashMessage(redirectAttributes, FlashMessageType.DANGER, "contentPage.deleteIndex.invalid", new String[]{});
			view = "redirect:/auth/admin/pages/" + id;
		} else {
			contentPageService.delete(new ContentPage(id));
			controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"page","deleted"});
			sessionStatus.setComplete();
			view = "redirect:/auth/admin/pages";
		}
		
		return view;
	}

	@Autowired
	public void setContentPageValidator(ContentPageValidator contentPageValidator) {
		this.contentPageValidator = contentPageValidator;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}
	
	@Autowired
	public void setContentPageService(ContentPageService contentPageService) {
		this.contentPageService = contentPageService;
	}
	
	@Autowired
	public void setContentPageContextService(
			ContentPageContextService contentPageContextService) {
		this.contentPageContextService = contentPageContextService;
	}

	private String prepareModel(ContentPage contentPage, Model model) {
		model.addAttribute("contentPage", contentPage);
		model.addAttribute("isNew", (contentPage.getId() == null));
		return "auth/admin/pages/view";		
	}
	
}
