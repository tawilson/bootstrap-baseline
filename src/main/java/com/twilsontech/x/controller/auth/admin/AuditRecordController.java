package com.twilsontech.x.controller.auth.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.twilsontech.x.controller.common.CustomPageRequest;
import com.twilsontech.x.controller.common.PaginatedListImpl;
import com.twilsontech.x.model.audit.AuditRecord;
import com.twilsontech.x.service.AuditRecordService;

@Controller
public class AuditRecordController {
	
	private AuditRecordService auditRecordService;

	@RequestMapping(value = { "/auth/admin/audit" })
	public String list(
			Model model,
			@PageableDefault(page = 0, size = 20, sort = { "timestamp" }, direction = Direction.DESC) Pageable pageable) {
		
		Page<AuditRecord> auditRecords = auditRecordService.findAll(new CustomPageRequest(pageable));
		model.addAttribute("auditRecordList", new PaginatedListImpl(auditRecords));
		
		return "auth/admin/audit";
	}

	@Autowired
	public void setAuditRecordService(AuditRecordService auditRecordService) {
		this.auditRecordService = auditRecordService;
	}

}
