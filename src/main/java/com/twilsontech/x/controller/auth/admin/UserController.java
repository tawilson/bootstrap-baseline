package com.twilsontech.x.controller.auth.admin;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.CustomPageRequest;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.controller.common.PaginatedListImpl;
import com.twilsontech.x.model.Authority;
import com.twilsontech.x.model.Authority.Role;
import com.twilsontech.x.model.User;
import com.twilsontech.x.model.validator.ValidatorUtils;
import com.twilsontech.x.service.UserService;

@Controller
@SessionAttributes("user")
public class UserController {

	private ControllerUtils controllerUtils;
	private UserService userService;
	private ValidatorUtils validatorUtils;
	
	private static final String USERS_URL = "/auth/admin/users";

	@RequestMapping(value = { USERS_URL })
	public String findAllUsers(
			Model model,
			@PageableDefault(page = 0, size = 20, sort = { "username" }, direction = Direction.ASC) Pageable pageable) {

		Page<User> users = userService.findByDeletedAndUsernameNot(false, User.SYSTEM_USERNAME, new CustomPageRequest(pageable));
		model.addAttribute("userList", new PaginatedListImpl(users));
		
		Page<User> deletedUsers = userService.findByDeletedAndUsernameNot(true, User.SYSTEM_USERNAME, new CustomPageRequest(new PageRequest(0, Integer.MAX_VALUE, Direction.ASC, "username")));
		model.addAttribute("deletedUsers", deletedUsers.getContent());		

		return "auth/admin/users/list";
	}

	@RequestMapping("/auth/admin/users/{id}")
	public String view(@PathVariable Integer id, Model model, Principal principal) {

		User user = userService.find(new User(id));
		return prepareModel(model, user, principal);
	}
	
	@RequestMapping("/auth/admin/users/create")
	public String create(Model model, Principal principal) {

		User user = new User();
		return prepareModel(model, user, principal);
	}
	
	@RequestMapping("/auth/admin/users/{id}/delete")
	public String delete(@PathVariable Integer id, Model model, RedirectAttributes redirectAttributes) {
		userService.deleteUser(new User(id));
		controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"user","deleted"});
		return "redirect:" + USERS_URL;
	}	

	@RequestMapping("/auth/admin/users/save")
	public String save(
			@RequestParam(value = "authority", required = false) Set<String> authorities,
			@ModelAttribute("user") User user,
			BindingResult bindingResult,
			Model model,
			RedirectAttributes redirectAttributes,
			SessionStatus sessionStatus,
			Principal principal) {
		
		user.getAuthorities().clear();
		
		if(user.isNew()) {
			validatorUtils.validateUniqueEmail(false, user.getUsername(), bindingResult, "username");
		}
		// Make sure the user has a role
		if (CollectionUtils.isEmpty(authorities)) {
			bindingResult.rejectValue("authorities", "valueRequired");			
		}		
		
		if (bindingResult.hasErrors()) {
			controllerUtils.addModelMessage(model, FlashMessageType.DANGER, "genericInvalidSubmission", new String[]{"user"});
			return prepareModel(model, user, principal);			
		} else {
					
			// bind selected authorities
			for (String authroity : authorities) {
				user.getAuthorities().add(new Authority(authroity, user));
			}
			if(user.isNew()) {
				user.setPassword(RandomStringUtils.randomAlphanumeric(6));
				userService.createUser(user, true);
			} else {
				userService.update(user);
			}			
			controllerUtils.addGenericFlashMessage(redirectAttributes,
					FlashMessageType.SUCCESS, new String[] { "user", "saved" });
			sessionStatus.setComplete();
			return "redirect:" + USERS_URL + "/" + user.getId();
		}

	}
	
	@RequestMapping("/auth/admin/users/{id}/undelete")
	public String unDelete(@PathVariable Integer id, Model model, RedirectAttributes redirectAttributes) {
		userService.unDeleteUser(new User(id));
		controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"user","un-deleted"});
		return "redirect:" + USERS_URL;
	}		

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	@Autowired
	public void setValidatorUtils(ValidatorUtils validatorUtils) {
		this.validatorUtils = validatorUtils;
	}

	private String prepareModel(Model model, User user, Principal principal) {
		model.addAttribute("user", user);
		Set<Authority> authorities = new HashSet<>();
		for (Role role : Role.values()) {
			if(!role.equals(Role.ROLE_SYSTEM)) {  // can't give someone the system role.
				authorities.add(new Authority(role.getName(), user));
			}
		}
		model.addAttribute("authorities", authorities);	
		
		boolean canDelete = true;
		if(user.isNew() || user.getUsername().equals(principal.getName()) || user.getUsername().equals(User.SYSTEM_USERNAME)) {
			canDelete = false;
		}
		model.addAttribute("canDelete", canDelete);
		model.addAttribute("newUser", !user.isNew());
		
		return "auth/admin/users/view";
	}	

}
