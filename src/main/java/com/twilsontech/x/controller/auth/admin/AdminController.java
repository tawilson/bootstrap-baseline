package com.twilsontech.x.controller.auth.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {


	@RequestMapping(value = {"/auth/admin", "/auth/admin/index"})
	public String index() {
		return "auth/admin/index";
	}
	
}
