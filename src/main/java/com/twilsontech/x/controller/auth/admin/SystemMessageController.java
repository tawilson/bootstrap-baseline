package com.twilsontech.x.controller.auth.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.SystemMessage;
import com.twilsontech.x.model.SystemMessage.Type;
import com.twilsontech.x.model.UserPageScope;

@Controller
public class SystemMessageController {
	
	public static final String SYSTEM_MESSAGE = "systemMessageInContext";
	
	private ControllerUtils controllerUtils;
	
	@RequestMapping("/auth/admin/systemMessage/view")
	public String view(Model model, HttpSession session) {
		
		SystemMessage systemMessage = (SystemMessage) session.getServletContext().getAttribute(SYSTEM_MESSAGE);
		
		if(systemMessage == null) {
			systemMessage = new SystemMessage();
		}
		
		model.addAttribute("systemMessage", systemMessage);
		return "auth/admin/systemMessage";
	}
	
	@RequestMapping("/auth/admin/systemMessage/save")
	public String save(SystemMessage systemMessage, RedirectAttributes redirectAttributes, HttpSession session) {
		
		session.getServletContext().setAttribute(SYSTEM_MESSAGE, systemMessage);
		
		controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"system message","saved"});
		
		return "redirect:/auth/admin/systemMessage/view";
	}	
	
	@ModelAttribute("messageScopes")
	public UserPageScope[] getScope() {
		return UserPageScope.values();
	}
	
	@ModelAttribute("messageTypes")
	public Type[] getTypes() {
		return SystemMessage.Type.values();
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}
	
}
