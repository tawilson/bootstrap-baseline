package com.twilsontech.x.controller.auth.profile;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/auth/profile/")
public class ProfileController {
	
	@RequestMapping(value = "/")
	public String home() {
		return "redirect:/auth/profile/password";
	}

}
