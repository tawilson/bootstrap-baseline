package com.twilsontech.x.controller.auth.admin;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.twilsontech.x.controller.command.AjaxStatus;
import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.CustomPageRequest;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.Media;
import com.twilsontech.x.service.MediaService;

@Controller
@RequestMapping("/auth/admin/media")
public class MediaController {
	
	private static final Logger logger = LoggerFactory.getLogger(MediaController.class);

	private MediaService mediaService;
	private MessageSource messageSource;
	private ControllerUtils controllerUtils;

	@RequestMapping(value = "")
	public String index(Model model) {
		if(!mediaService.isMediaDirectoryWritable()) {
			controllerUtils.addModelMessage(model, FlashMessageType.WARNING, "media.directoryUnwritable", new String[]{mediaService.getMediaDirectory()});
		}
		return "auth/admin/media/list";
	}

	@RequestMapping("/save")
	@ResponseBody
	public AjaxStatus saveMedia(@RequestParam("file") MultipartFile file) {
		AjaxStatus ajaxStatus;
		try {

			// For now no duplicate file names allowed.
			if (mediaService.findByOriginalFilename(file.getOriginalFilename()) == null) {

				Media media = new Media(file.getOriginalFilename(),
						file.getContentType(), file.getSize(), file.getBytes());
				mediaService.save(media);

				ajaxStatus = new AjaxStatus(true);
				ajaxStatus.getData().put("media",media);
			} else {
				ajaxStatus = new AjaxStatus(false);
				ajaxStatus.setMessage(messageSource.getMessage("media.filename.exists", null, Locale.getDefault()));
			}
		} catch (Exception e) {
			logger.error("Unable to save Media Object.", e);
			ajaxStatus = new AjaxStatus(false);
		}

		return ajaxStatus;
	}
	
	@RequestMapping("/list")
	@ResponseBody	
	public AjaxStatus listMedia(@PageableDefault(page = 0, size = Integer.MAX_VALUE, sort = { "createDate" }, direction = Direction.DESC) Pageable pageable) {
		Page<Media> mediaResults = mediaService.findAll(new CustomPageRequest(pageable));
		AjaxStatus ajaxStatus = new AjaxStatus(true);
		ajaxStatus.getData().put("mediaList", mediaResults.getContent());
		return ajaxStatus;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public @ResponseBody AjaxStatus deleteMedia(@PathVariable Integer id) {		
		mediaService.delete(new Media(id));
		AjaxStatus ajaxStatus = new AjaxStatus(true);		
		return ajaxStatus;
	}	

	@Autowired
	public void setMediaService(MediaService mediaService) {
		this.mediaService = mediaService;
	}

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}

}
