package com.twilsontech.x.controller.auth.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.controller.command.AjaxStatus;
import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.ApplicationProperty;
import com.twilsontech.x.service.ApplicationPropertiesService;

@Controller
public class AppPropsController {

	private ApplicationPropertiesService applicationPropertiesService;
	private ControllerUtils controllerUtils; 

	@RequestMapping("/auth/admin/appProps")
	public String list(Model model) {
		model.addAttribute("appPropList", applicationPropertiesService.findAll());
		return "auth/admin/appProps";
	}

	@RequestMapping(value = "/auth/admin/appProps/save")
	public String save(@RequestParam("appPropId") Integer appPropId, @RequestParam("") String appPropValue, RedirectAttributes redirectAttributes,
			HttpSession session) {
		
		ApplicationProperty applicationProperty = applicationPropertiesService.findOne(appPropId);
		applicationProperty.setValue(appPropValue);
		
		applicationPropertiesService.update(applicationProperty);
		session.getServletContext().setAttribute(applicationProperty.getKey(), applicationProperty.getValue());
		
		controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"property","saved"});
		
		return "redirect:/auth/admin/appProps";
	}
	
	@RequestMapping("/auth/admin/appProps/{id}")
	@ResponseBody
	public AjaxStatus findOne(@PathVariable Integer id) {
		ApplicationProperty applicationProperty = applicationPropertiesService.findOne(id);
		AjaxStatus ajaxStatus = new AjaxStatus(true);
		ajaxStatus.getData().put("applicationProperty", applicationProperty);
		return ajaxStatus;
	}

	@Autowired
	public void setApplicationPropertiesService(
			ApplicationPropertiesService applicationPropertiesService) {
		this.applicationPropertiesService = applicationPropertiesService;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}

}
