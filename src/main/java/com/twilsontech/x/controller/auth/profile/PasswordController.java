package com.twilsontech.x.controller.auth.profile;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.ResetPassword;
import com.twilsontech.x.model.validator.ResetPasswordValidator;
import com.twilsontech.x.service.UserService;

@Controller
public class PasswordController {
	
	private ControllerUtils controllerUtils;
	private ResetPasswordValidator resetPasswordValidator;
	private UserService userService;
	
	@RequestMapping(value = "/auth/profile/password", method = RequestMethod.GET)
	public String view(Model model) {
		return prepareForm(new ResetPassword(), model);
	}
	
	@RequestMapping(value = "/auth/profile/password", method = RequestMethod.POST)
	public String update(@ModelAttribute ResetPassword resetPassword, Model model,
			BindingResult bindingResult, RedirectAttributes redirectAttributes,
			Principal principal) {
		
		UserDetails userDetails = controllerUtils.getUserDetails(principal);
		resetPassword.setUsername(userDetails.getUsername());
		resetPasswordValidator.validate(resetPassword, bindingResult);
		
		if(bindingResult.hasErrors()) {
			resetPassword.clearPasswords();
			controllerUtils.addModelMessage(model, FlashMessageType.DANGER, "genericInvalidSubmission", new String[]{"request"});
			return prepareForm(resetPassword, model);
		} else {
			userService.updatePassword(resetPassword);		
			controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"password", "updated"});
			return "redirect:/auth/profile/password";
		}
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}

	@Autowired
	public void setResetPasswordValidator(
			ResetPasswordValidator resetPasswordValidator) {
		this.resetPasswordValidator = resetPasswordValidator;
	}	
	
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	private String prepareForm(ResetPassword resetPassword, Model model) {
		model.addAttribute("resetPassword", resetPassword);
		return "auth/profile/password";
	}
	
}
