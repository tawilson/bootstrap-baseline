package com.twilsontech.x.controller.auth;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class KeepSessionAliveController {
	
	private static final Log logger = LogFactory.getLog(KeepSessionAliveController.class);
	
	@RequestMapping("/auth/tickleSession")
	public void tickle() {
		logger.debug("Session tickled.");
	}

}
