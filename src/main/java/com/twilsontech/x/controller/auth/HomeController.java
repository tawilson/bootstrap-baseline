package com.twilsontech.x.controller.auth;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping(value = {"/auth/", "/auth/home"})
	public String home() {
		return "auth/home";
	}
	
}
