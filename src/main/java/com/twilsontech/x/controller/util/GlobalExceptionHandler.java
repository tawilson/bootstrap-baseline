package com.twilsontech.x.controller.util;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.twilsontech.x.controller.ExceptionController;
import com.twilsontech.x.service.ExceptionForwardingService;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);	
	
	private ExceptionForwardingService exceptionForwardingService;	
	
	@ExceptionHandler(Exception.class)
	public void defaultExceptionHandler(Principal principal, Exception exception, HttpServletRequest request) throws Exception {
		
		String userEnvInfo = RequestUtils.getUserEnvInfo(request);
		
		logger.error("*************** EXCEPTION ******************");
		logger.error(request.getRequestURI());
		logger.error(userEnvInfo);
		logger.error(exception.getMessage(),exception);
		logger.error("*********************************");

		exceptionForwardingService.forward(principal, exception,request.getRequestURI(),userEnvInfo);		
		
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it.
        if (AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class) != null)
            throw exception;		
		
        throw exception;
	}

	@Autowired
	public void setExceptionForwardingService(
			ExceptionForwardingService exceptionForwardingService) {
		this.exceptionForwardingService = exceptionForwardingService;
	}

}
