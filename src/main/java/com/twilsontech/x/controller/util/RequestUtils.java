package com.twilsontech.x.controller.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

public class RequestUtils {
	
	public static String getUserEnvInfo(HttpServletRequest request) {
		StringBuilder headerBuilder = new StringBuilder();
		
		headerBuilder.append("[host:").append(request.getRemoteHost()).append("]");
		headerBuilder.append("[ip:").append(request.getRemoteAddr()).append("]");
		
		Enumeration<String> headerNames = request.getHeaderNames();
		
		while(headerNames.hasMoreElements()) {
			String name = headerNames.nextElement();
			headerBuilder.append("[").append(name).append(":").append(request.getHeader(name));
		}
		
		return headerBuilder.toString();
	}

}
