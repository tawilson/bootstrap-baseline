package com.twilsontech.x.controller.samples;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twilsontech.x.model.Message;
import com.twilsontech.x.service.samples.MessageService;

@Controller
@RequestMapping("/samples/tokeninput")
public class TokenInputController {
	
	private MessageService messageService;

	@RequestMapping("/")
	public String view() {
		return "samples/tokeninput";
	}

	@RequestMapping("/search")
	public @ResponseBody List<Message> search(@RequestParam("q") String query) {
		List<Message> messages = messageService.findByMessageContaining(query);
		return messages;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

}
