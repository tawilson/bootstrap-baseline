package com.twilsontech.x.controller.samples;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/samples")
public class SamplesController {
	
	@RequestMapping("/")
	public String samples() {
		return "samples/index";
	}


}
