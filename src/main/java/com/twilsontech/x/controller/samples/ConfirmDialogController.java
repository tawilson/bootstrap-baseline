package com.twilsontech.x.controller.samples;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;

@Controller
public class ConfirmDialogController {
	
	private ControllerUtils controllerUtils;
	
	@RequestMapping("/samples/confirmDialog")
	public String view() {
		return "samples/confirmDialog";
	}
	
	@RequestMapping("/samples/confirmDialog/redirect")
	public String redirectExample(RedirectAttributes redirectAttributes) {
		controllerUtils.addFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, "adhocMessage", new String[]{"Redirection was successful."});
		return "redirect:/samples/confirmDialog";
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}	
	
}
