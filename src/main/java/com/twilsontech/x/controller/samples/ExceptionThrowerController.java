package com.twilsontech.x.controller.samples;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.twilsontech.x.common.exception.DefaultException;

@Controller
@RequestMapping("/samples/throw")
public class ExceptionThrowerController {
	
	@RequestMapping("/default")
	public void throwDefault() {
		throw new DefaultException("Boom!");
	}

}
