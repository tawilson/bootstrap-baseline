package com.twilsontech.x.controller.samples;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.common.exception.DefaultException;
import com.twilsontech.x.common.exception.ResourceNotFoundException;
import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.Message;
import com.twilsontech.x.model.validator.MessageValidator;
import com.twilsontech.x.service.samples.MessageService;

@Controller
@RequestMapping("/samples/message")
@SessionAttributes(value="message")
public class MessageController {
	
	private ControllerUtils controllerUtils;
	private MessageValidator messageValidator;
	private MessageService messageService;
	
	@RequestMapping("/")
	public String viewNew(Model model) {
		Message message = new Message();
		return prepareModel(message, model);
	}
	
	@RequestMapping("/{id}")
	public String view(@PathVariable Integer id, Model model) {
		Message message = messageService.findOne(id);
		
		if(message == null) {
			throw new ResourceNotFoundException();
		}
		
		return prepareModel(message, model);
	}
	
	@RequestMapping("/{id}/delete")
	public String delete(@PathVariable Integer id, Model model, SessionStatus sessionStatus, RedirectAttributes redirectAttributes) {
		Message message = messageService.findOne(id);
		messageService.delete(message);
		controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"message","deleted"});
		sessionStatus.setComplete();
		return "redirect:/samples/message/";
	}	
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public String saveMessage(@ModelAttribute Message message, BindingResult bindingResult, Model model, SessionStatus sessionStatus, RedirectAttributes redirectAttributes) {
		messageValidator.validate(message, bindingResult); // just a sanity check.  Validation will be done client side where possible.
		if(!bindingResult.hasErrors()) {
			messageService.save(message);
			controllerUtils.addGenericFlashMessage(redirectAttributes, FlashMessageType.SUCCESS, new String[]{"message","saved"});
			sessionStatus.setComplete();
			return "redirect:/samples/message/" + message.getId();
		} else {
			controllerUtils.addModelMessage(model, FlashMessageType.DANGER, "genericInvalidSubmission", new String[]{"message"});
			return prepareModel(message, model);
		}
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) throws Exception {
		// TODO: Make this a util
		SimpleDateFormat sdf = new SimpleDateFormat("h:mm a, M/d/yyyy");
	    sdf.setLenient(true);		
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));		
	}		
	

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}
	
	@Autowired
	public void setMessageValidator(MessageValidator messageValidator) {
		this.messageValidator = messageValidator;
	}
	
	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}

	private String prepareModel(Message message, Model model) {
		model.addAttribute("message", message);
		model.addAttribute("isNew", message.getId() == null);
		return "samples/message";
	}
	
}
