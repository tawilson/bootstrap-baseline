package com.twilsontech.x.controller.samples;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.twilsontech.x.model.Message;
import com.twilsontech.x.samples.CalendarEvent;
import com.twilsontech.x.samples.CalendarEventImpl;
import com.twilsontech.x.service.samples.MessageService;

@Controller
@RequestMapping("/samples")
public class FullCalendarController {
	
	private MessageService messageService;
	
	public FullCalendarController() {		
	}
	
	@RequestMapping("/fullCalendar")
	public String fullCalendar(Model model) {
		return "samples/fullCalendar";
	}
	
	@RequestMapping(value = "/fullCalendar/fetch", produces = "application/json")
	public @ResponseBody List<CalendarEvent> fullCalendarFetch(@RequestParam("start") Long start, @RequestParam("end") Long end, Model model, HttpServletRequest request) throws Exception {
		
		List<Message> messages = messageService.findByTimestampBetween(new Date(start*1000), new Date(end*1000));
		List<CalendarEvent> calendarEvents = new ArrayList<>();
		
		for(Message message : messages) {
			calendarEvents.add(new CalendarEventImpl(
					message.getId().toString(), 
					request.getContextPath() + "/samples/message/" +  message.getId(), 
					message.getMessage(), 
					new DateTime(message.getCreated())));
		}
		
		return calendarEvents;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}
	
}
