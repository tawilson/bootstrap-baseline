package com.twilsontech.x.controller.samples;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.twilsontech.x.controller.common.CustomPageRequest;
import com.twilsontech.x.controller.common.PaginatedListImpl;
import com.twilsontech.x.model.Message;
import com.twilsontech.x.service.samples.MessageService;

@Controller
@RequestMapping("/samples")
public class DisplayTagController {

	private MessageService messageService;

	public DisplayTagController() {
	}

	@RequestMapping("/displayTag")
	public String displayTag(
			Model model,
			@PageableDefault(page = 0, size = 20, sort = { "created" }, direction = Direction.ASC) Pageable pageable) {
		
		Page<Message> messageResults = messageService.findAll(new CustomPageRequest(pageable));

		model.addAttribute("messageList", new PaginatedListImpl(messageResults));

		return "samples/displayTag";
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

}
