package com.twilsontech.x.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.User;
import com.twilsontech.x.model.validator.RegisterValidator;
import com.twilsontech.x.service.UserService;

@Controller
@SessionAttributes("user")
public class RegistrationController {
	
	private UserService userService;
	private RegisterValidator registerValidator;
	private ControllerUtils controllerUtils;

	@RequestMapping(value="/register", method = RequestMethod.GET)
	public String view(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}
	
	@RequestMapping(value="/register", method = RequestMethod.POST)
	public String register(@ModelAttribute("user") User user, BindingResult bindingResult, Model model, SessionStatus sessionStatus) {
		
		registerValidator.validate(user, bindingResult);
		if(bindingResult.hasErrors()) {			
			user.setPassword(null);
			model.addAttribute("user", user);	
			controllerUtils.addModelMessage(model, FlashMessageType.DANGER, "genericInvalidSubmission", new String[]{"account information"});
			return "register";	
			
		} else {
			userService.registerUser(user);
			sessionStatus.setComplete();
			controllerUtils.authenticateUser(user.getUsername(), user.getPassword());
			return "redirect:/auth/registerSuccess";
		}
			
	}
	
	@RequestMapping("/auth/registerSuccess")
	public String registerSuccess() {
		return "auth/registerSuccess";
	}

	@Autowired
	public void setRegisterValidator(RegisterValidator registerValidator) {
		this.registerValidator = registerValidator;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}	
	
}
