package com.twilsontech.x.controller.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.twilsontech.x.model.UserPageScope;

public class UserPageScopeInterceptor extends HandlerInterceptorAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(UserPageScopeInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		UserPageScope scope = UserPageScope.MAIN;
		
		if(request.getRequestURI().contains("/auth/")) {
			scope = UserPageScope.AUTH;
		}
		
		logger.debug("User Scope: {}", scope);
		
		request.setAttribute("userPageScope", scope);
		
		return super.preHandle(request, response, handler);
	}
	
	

}
