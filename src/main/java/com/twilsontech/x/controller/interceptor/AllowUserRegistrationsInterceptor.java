package com.twilsontech.x.controller.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AllowUserRegistrationsInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		boolean allow = true;
		
		String allowUserRegistrations = (String) request.getSession().getServletContext().getAttribute("allowUserRegistrations");
		if(StringUtils.isNotEmpty(allowUserRegistrations) && allowUserRegistrations.equals("false")) {
			allow = false;						
			response.sendRedirect(request.getContextPath());
		}

		return allow;
	}
	
	

}
