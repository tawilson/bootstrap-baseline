package com.twilsontech.x.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.LostPassword;
import com.twilsontech.x.model.validator.ValidatorUtils;
import com.twilsontech.x.service.UserService;

@Controller
public class LostPasswordController {
	
	private UserService userService;
	private ValidatorUtils validatorUtils;
	private ControllerUtils controllerUtils;
	
	@RequestMapping(value="/lostpassword", method=RequestMethod.GET)	
	public String view(HttpServletRequest request, ModelMap model) {		
		model.addAttribute("lostPassword", new LostPassword());		
		return "lostpassword";
	}
	
	@RequestMapping(value="/lostpassword", method=RequestMethod.POST)	
	public String submit(@ModelAttribute LostPassword lostPassword, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {		
		
		validatorUtils.validateUniqueEmail(true, lostPassword.getUsername(), bindingResult, "username");
		if(bindingResult.hasErrors()) {
			model.addAttribute("lostPassword", new LostPassword());	
			controllerUtils.addModelMessage(model, FlashMessageType.DANGER, "email.invalid", new String[]{});		
			return "lostpassword";
		} else {
			userService.sendPasswordReset(lostPassword.getUsername());
			controllerUtils.addFlashMessage(redirectAttributes,
					FlashMessageType.SUCCESS, "lostPassword.submit.message",
					new String[] {});
			return "redirect:/";			
		}
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setValidatorUtils(ValidatorUtils validatorUtils) {
		this.validatorUtils = validatorUtils;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}	
	
}
