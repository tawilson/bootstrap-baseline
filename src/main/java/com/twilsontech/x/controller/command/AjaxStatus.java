package com.twilsontech.x.controller.command;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Switch to alert type enum.
 */
public class AjaxStatus {

	private boolean success;
	private String message;
	private String alertType;
	
	private Map<String, Object> data = new HashMap<String, Object>();
	
	public AjaxStatus() {
	}

	public AjaxStatus(boolean success) {	
		this.success = success;
		
		if(success) {
			message = "Success";
			alertType = "alert-success";
		} else {
			message = "Failure";
			alertType = "alert-danger";
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}		
	
	

}
