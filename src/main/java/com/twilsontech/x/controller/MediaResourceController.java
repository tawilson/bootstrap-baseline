package com.twilsontech.x.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.twilsontech.x.common.exception.ResourceNotFoundException;
import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.model.Media;
import com.twilsontech.x.service.MediaService;

@Controller
public class MediaResourceController {

	private MediaService mediaService;
	private ControllerUtils controllerUtils;

	/**
	 * Spring strips the training period from the file name.
	 * @param originalFileName
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping("/media/{originalFileName}")
	public void getMedia(@PathVariable String originalFileName,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String requestPath = request.getServletPath();
		String fullFileName =  requestPath.substring(requestPath.lastIndexOf('/') + 1).split("\\?")[0].split("#")[0];		
		Media media = mediaService.findByOriginalFilename(fullFileName);
		if (media != null) {
			response.setContentType(media.getContentType());
			response.setContentLength((int) media.getSize());
			mediaService.copyMediaToOutputStream(media, response.getOutputStream());
			controllerUtils.putCacheHeaders(response, media.getLastUpdate());
		} else {
			throw new ResourceNotFoundException();
		}
	}

	@Autowired
	public void setMediaService(MediaService mediaService) {
		this.mediaService = mediaService;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}
	
}
