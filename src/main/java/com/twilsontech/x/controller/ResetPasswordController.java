package com.twilsontech.x.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.twilsontech.x.common.exception.DefaultException;
import com.twilsontech.x.controller.common.ControllerUtils;
import com.twilsontech.x.controller.common.FlashMessageType;
import com.twilsontech.x.model.User;
import com.twilsontech.x.model.ResetPassword;
import com.twilsontech.x.model.validator.ResetPasswordValidator;
import com.twilsontech.x.service.UserService;

@Controller
@SessionAttributes("resetPassword")
public class ResetPasswordController {
	
	private static final Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);
	
	private UserService userService;
	private ResetPasswordValidator resetPasswordValidator;
	private ControllerUtils controllerUtils;
	
	@RequestMapping("/resetpassword") // the callback from the email
	public String resetPassword(@RequestParam("e") String email,
			@RequestParam("t") String token,
			HttpServletRequest request) {				
		
		String viewName = null;
		boolean invalidParamaters = false;								
		
		if(GenericValidator.isEmail(email) && 
		   !GenericValidator.isBlankOrNull(token) &&
		   GenericValidator.maxLength(token, 100)) {
			
			User user = userService.resetPasswordValidate(email, token);
			
			if(user != null) {
				ResetPassword command = new ResetPassword();
				command.setUsername(user.getUsername());
				command.setAuthToken(token);				
				request.getSession().setAttribute("resetpasswordmodel", command);
				viewName = "redirect:/resetpasswordform";				
			} else {
				invalidParamaters = true;
			}
		} else {
			invalidParamaters = true;
		}
		
		if(invalidParamaters) {
			logger.error("Invalid params for password reset {}, {}", email, token);
			throw new DefaultException();
		}

		return viewName;		
	}
		
	@RequestMapping(value="/resetpasswordform",method=RequestMethod.GET)
	public String getResetPasswordForm(Model model, HttpServletRequest request) {		
		
		// the user should only arrive here by following the original link sent by the app.  Following this link will put the 
		ResetPassword resetPassword = (ResetPassword) request.getSession().getAttribute("resetpasswordmodel");
		request.getSession().removeAttribute("resetpasswordmodel");
		
		if(resetPassword == null) {		
			throw new DefaultException();
		}
				
		return prepareForm(resetPassword, model);
	}		
	
	@RequestMapping(value="/resetpasswordform",method=RequestMethod.POST)
	protected String submitResetPasswordForm(@ModelAttribute("resetPassword") ResetPassword resetPassword, Model model,
			BindingResult bindingResult, RedirectAttributes redirectAttributes, SessionStatus sessionStatus, HttpSession session) {
				
		resetPasswordValidator.validate(resetPassword, bindingResult);
		
		if(bindingResult.hasErrors()) {
			controllerUtils.addModelMessage(model, FlashMessageType.DANGER, "genericInvalidSubmission", new String[]{"request"});		
			return prepareForm(resetPassword, model);
		} else {			
			userService.externalResetPassword(resetPassword);	
			controllerUtils.authenticateUser(resetPassword.getUsername(), resetPassword.getPassword());
			// results in a bunch of redirects, so best to add as a session message
			controllerUtils.addSessionMessage(session, FlashMessageType.SUCCESS, "genericFlashMessage", new String[]{"password", "updated"});
			sessionStatus.setComplete();
			return "redirect:/auth/";
		}

	}	

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setResetPasswordValidator(
			ResetPasswordValidator resetPasswordValidator) {
		this.resetPasswordValidator = resetPasswordValidator;
	}

	@Autowired
	public void setControllerUtils(ControllerUtils controllerUtils) {
		this.controllerUtils = controllerUtils;
	}
	
	private String prepareForm(ResetPassword resetPassword, Model model) {
		resetPassword.clearPasswords();
		model.addAttribute("resetPassword", resetPassword);
		return "resetpassword";
	}
	
}
