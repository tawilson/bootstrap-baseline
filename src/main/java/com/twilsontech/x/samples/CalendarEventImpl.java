package com.twilsontech.x.samples;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;

/**
 * TODO: Use the builder pattern.
 * @author tariq
 *
 */
public class CalendarEventImpl implements CalendarEvent {

	private String id;
	private String url;
	private String title;
	private DateTime date;
	private DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendDayOfWeekText().toFormatter();

	public CalendarEventImpl(String id, String url, String title, DateTime date) {
		super();
		this.id = id;
		this.url = url;
		this.title = title;
		this.date = date;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getStart() {
		return date.toString(ISODateTimeFormat.dateTime());
	}

	@Override
	public String getEnd() {
		return null;
	}

	@Override
	public String getBackgroundColor() {
		return null;
	}

	@Override
	public String getBorderColor() {
		return null;
	}

	@Override
	public String getTextColor() {
		return null;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public boolean getAllDay() {
		return false;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

}
