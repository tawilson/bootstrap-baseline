package com.twilsontech.x.samples;

public interface CalendarEvent {
	
	public String getId();

	public String getTitle();

	public String getStart();
	
	public String getEnd();	
	
	public String getBackgroundColor();
	
	public String getBorderColor();	
	
	public String getTextColor();

	public String getUrl();
	
	public boolean getAllDay();

}
