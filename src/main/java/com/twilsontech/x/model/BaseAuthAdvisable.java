package com.twilsontech.x.model;

import javax.persistence.Transient;

import com.twilsontech.x.model.auth.AuthAdvisable;

public abstract class BaseAuthAdvisable extends BaseAbstractModel implements
		AuthAdvisable {

	protected ResourceType resourceType;

	public BaseAuthAdvisable(ResourceType resourceType) {
		super();
		this.resourceType = resourceType;
	}

	@Transient
	@Override
	public String getAuthKey() {
		return id.toString();
	}

	@Transient
	@Override
	public ResourceType getResourceType() {
		return resourceType;
	}

}