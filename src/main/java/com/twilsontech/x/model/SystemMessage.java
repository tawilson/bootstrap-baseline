package com.twilsontech.x.model;

public class SystemMessage {

	public enum Type {
		primary, success, info, warning, danger
	}

	private String message;
	private UserPageScope scope;
	private Type type;
	private boolean enabled;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UserPageScope getScope() {
		return scope;
	}

	public void setScope(UserPageScope scope) {
		this.scope = scope;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
