package com.twilsontech.x.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CONTENT_PAGE_CONTEXT")
public class ContentPageContext extends BaseAuthAdvisable {
	
	private ContentPage indexPage;

	public ContentPageContext() {
		super(ResourceType.CONTENT_PAGE_CONTEXT);
	}

	@ManyToOne
	public ContentPage getIndexPage() {
		return indexPage;
	}

	public void setIndexPage(ContentPage indexPage) {
		this.indexPage = indexPage;
	}
	
}
