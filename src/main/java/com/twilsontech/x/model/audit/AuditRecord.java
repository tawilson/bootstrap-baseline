package com.twilsontech.x.model.audit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.twilsontech.x.model.BaseAbstractModel;
import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.User;

@Entity
@Table(name = "AUDIT_RECORD")
public class AuditRecord extends BaseAbstractModel {

	private User user;
	private ResourceType resourceType;
	private String resourceId;
	private DomainAction action;
	private Date timestamp;
	
	public AuditRecord() {
		super();
	}
	
	public AuditRecord(User user, ResourceType resourceType, String resourceId,
			DomainAction action) {
		super();
		this.user = user;
		this.resourceType = resourceType;
		this.resourceId = resourceId;
		this.action = action;
		this.timestamp = new Date();
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID", nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Enumerated(EnumType.STRING)	
	public ResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}

	@Column(nullable = true)
	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@Enumerated(EnumType.STRING)
	public DomainAction getAction() {
		return action;
	}

	public void setAction(DomainAction action) {
		this.action = action;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
