package com.twilsontech.x.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "APPLICATION_PROPERTIES")
public class ApplicationProperty extends BaseAuthAdvisable {

	private String key;
	private String value;
	private String type;
	
	public ApplicationProperty() {
		super(ResourceType.APP_PROPS);
	}
	
	public ApplicationProperty(String key, String value, String type) {
		this();
		this.key = key;
		this.value = value;
		this.type = type;
	}

	@Column(name = "PROP_KEY", length = 100)
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Column(name = "PROP_VALUE")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Typically the class type that the object can be cast to.
	 * @return
	 */
	@Column(name = "PROP_TYPE", length = 400)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
