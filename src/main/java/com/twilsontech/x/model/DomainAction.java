package com.twilsontech.x.model;

public enum DomainAction {
	
	/* Common Actions */
	CREATE, READ, UPDATE, DELETE, LIST, UN_DELETE
	
}
