package com.twilsontech.x.model.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.Validator;

public abstract class BaseValidator implements Validator {
	
	protected Class<?> objectClass;
	protected ValidatorUtils validatorUtils;
	protected MessageSource messageSource;
	
	public BaseValidator(Class<?> objectClass) {
		super();
		this.objectClass = objectClass;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(objectClass);
	}

	@Autowired
	public void setValidatorUtils(ValidatorUtils validatorUtils) {
		this.validatorUtils = validatorUtils;
	}

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
