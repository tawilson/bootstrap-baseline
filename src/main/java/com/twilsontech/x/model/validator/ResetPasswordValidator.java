package com.twilsontech.x.model.validator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.twilsontech.x.model.ResetPassword;
import com.twilsontech.x.service.UserService;

@Component
public class ResetPasswordValidator extends BaseValidator {
	
	private UserService userService;

	public ResetPasswordValidator() {
		super(ResetPassword.class);
	}

	public void validate(Object command, Errors errors) {
		
		ResetPassword resetPassword = (ResetPassword) command;
		
		// lack of an auth token means there should supply an auth token
		if(StringUtils.isEmpty(resetPassword.getAuthToken())) {
			if(GenericValidator.isBlankOrNull(resetPassword.getCurrentPassword())) {
				errors.rejectValue("currentPassword","valueRequired");
			} else {
				if(!userService.isCorrectPassword(resetPassword.getUsername(), resetPassword.getCurrentPassword())) {
					errors.rejectValue("currentPassword","improperValue");
				}
			}
		}
		
		validatorUtils.validatePassword(errors, resetPassword.getPassword(), "password");
		validatorUtils.validatePassword(errors, resetPassword.getPassword2(),"password2");
		
		if(!errors.hasErrors()) {
			if(!resetPassword.getPassword().equals(resetPassword.getPassword2())) {
				errors.rejectValue("password", "resetpassword.password.mismatch");
			}
		}

	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

		
}
