package com.twilsontech.x.model.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.twilsontech.x.model.User;

@Component
public class RegisterValidator extends BaseValidator {

	public RegisterValidator() {
		super(User.class);
	}

	@Override
	public void validate(Object o, Errors errors) {
		User newJpaUser = (User) o;
		validatorUtils.validateUniqueEmail(false, newJpaUser.getUsername(), errors, "username");
		validatorUtils.validatePassword(errors, newJpaUser.getPassword(), "password");
	}
	
}
