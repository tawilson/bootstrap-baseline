package com.twilsontech.x.model.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.twilsontech.x.model.Message;
import com.twilsontech.x.service.samples.MessageService;

@Component
public class MessageValidator extends BaseValidator {
	
	private MessageService messageService;

	public MessageValidator() {
		super(Message.class);
	}

	@Override
	public void validate(Object message, Errors errors) {
		int max = 250;
		if(messageService.count() >= max) {
			errors.reject("adhocMessage",new String[]{"Maximum number of messages (" + max + ") created.  Delete one first."}, null);
		}
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}
	
}
