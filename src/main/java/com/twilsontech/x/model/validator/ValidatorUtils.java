package com.twilsontech.x.model.validator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import com.twilsontech.x.model.User;
import com.twilsontech.x.service.UserService;


@Service
public class ValidatorUtils {
	
	private UserService userService;
	
	public static final String DEFAULT_ERROR_MESSAGE = "There is a problem with your entry.";	
	
	public void validateAgainstRegex(Errors errors,String regex, String value, String propertyLabel, String propertyName, String errorMessage) {
		if(!GenericValidator.matchRegexp(value, regex)) {
			errors.rejectValue(propertyName, errorMessage, 
					new String[]{}, DEFAULT_ERROR_MESSAGE);
		}			
	}
	
	public void validateNotEmptyAndLength(Errors errors, String value, 
			Integer length, String propertyLabel, String propertyName) {
		
		String regex = "^[a-zA-Z0-9\\s_-]{1," + length + "}$";
		
		if(!GenericValidator.matchRegexp(value, regex)) {
			errors.rejectValue(propertyName, "string.invalid", 
					new String[]{propertyLabel,length.toString()}, DEFAULT_ERROR_MESSAGE);
		}					
	}
	
	public void validateSafeAndLength(Errors errors, String value, 
			Integer length, String propertyLabel, String propertyName) {
		
		String regex = "^[a-zA-Z0-9\\s_-]{0," + length + "}$";
		
		if(!GenericValidator.matchRegexp(value, regex)) {
			errors.rejectValue(propertyName, "string.invalid", 
					new String[]{propertyLabel,length.toString()}, DEFAULT_ERROR_MESSAGE);
		}				
	}	
	
	public void validatePassword(Errors errors, String password, String propertyName) {			
		if(! ((password.length() >= 6) && (password.length() <= 12))) {
			errors.rejectValue(propertyName, "password.invalid", DEFAULT_ERROR_MESSAGE);
		}
	}	
	
	public void validateUniqueEmail(boolean mustExist, String emailAddress, Errors errors, String propertyName) {
		if(StringUtils.isBlank(emailAddress) || !GenericValidator.isEmail(emailAddress)) {
			errors.rejectValue(propertyName, "email.invalid");
		} else {					
			try {
				if(userService.loadUserByUsername(emailAddress) != null) {
					if(!mustExist) {
						errors.rejectValue(propertyName, "email.exists");
					}
				}
			} catch (UsernameNotFoundException e) {
				if(mustExist) {
					errors.rejectValue(propertyName, "email.invalid");					
				}
			}
		}			
	}
	

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
}
