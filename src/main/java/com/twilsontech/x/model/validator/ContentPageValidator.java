package com.twilsontech.x.model.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.twilsontech.x.model.ContentPage;
import com.twilsontech.x.service.ContentPageService;

@Component
public class ContentPageValidator extends BaseValidator {
	
	private ContentPageService contentPageService;
	
	public ContentPageValidator() {
		super(ContentPage.class);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ContentPage contentPage = (ContentPage) o;
		validatorUtils.validateNotEmptyAndLength(errors, contentPage.getBrowserTitle(), 100, messageSource.getMessage("contentPage.browserTitle", null, null), "browserTitle");
		validatorUtils.validateNotEmptyAndLength(errors, contentPage.getPageTitle(), 100, messageSource.getMessage("contentPage.pageTitle", null, null), "pageTitle");
		validatorUtils.validateAgainstRegex(errors, "^[a-zA-Z0-9-]{1,100}$", contentPage.getUrlPath(), "contentPage.urlPath", "urlPath", "contentPage.urlPath.invalid");
		
		if(!errors.hasErrors()) {
			ContentPage savedContentPage = contentPageService.findByUrlPath(contentPage.getUrlPath());
			if((savedContentPage != null) && !savedContentPage.getId().equals(contentPage.getId())) {
				errors.rejectValue("urlPath", "contentPage.urlPath.invalid.duplicate");
			}
		}
	}

	@Autowired
	public void setContentPageService(ContentPageService contentPageService) {
		this.contentPageService = contentPageService;
	}
	
}
