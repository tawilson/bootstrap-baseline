package com.twilsontech.x.model;

public class ResetPassword {
	
	private String currentPassword;
	private String password;
	private String password2;
	private String username;
	private String authToken;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String token) {
		this.authToken = token;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	
	public void clearPasswords() {
		currentPassword = "";
		password = "";
		password2 = "";
	}
	
}
