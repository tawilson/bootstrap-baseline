package com.twilsontech.x.model.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResourceType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Authorize {
	
	DomainAction action();
	
	ResourceType resourceType();
	
	/**
	 * Do we want to save an audit record for the action.
	 */
	boolean auditable() default true;

}
