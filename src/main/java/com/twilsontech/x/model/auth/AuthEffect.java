package com.twilsontech.x.model.auth;

public enum AuthEffect {
	
	ALLOW, DENY

}
