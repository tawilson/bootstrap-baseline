package com.twilsontech.x.model.auth;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.twilsontech.x.model.BaseAbstractModel;
import com.twilsontech.x.model.DomainAction;

@Entity
@Table(name = "AUTH_CONTEXT")
public class AuthorizationContext extends BaseAbstractModel {

	private String principal;
	private String resourceType;
	private String resourceId;
	private DomainAction action;

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@Enumerated(EnumType.STRING)
	public DomainAction getAction() {
		return action;
	}

	public void setAction(DomainAction action) {
		this.action = action;
	}

}
