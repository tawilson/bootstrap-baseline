package com.twilsontech.x.model.auth;

import com.twilsontech.x.model.ResourceType;

public interface AuthAdvisable {
	
	public String getAuthKey();
	
	public ResourceType getResourceType();

}
