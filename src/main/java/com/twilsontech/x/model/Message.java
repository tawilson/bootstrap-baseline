package com.twilsontech.x.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MESSAGES")
public class Message extends BaseAbstractModel {

	private Date created;
	private String message;
	private Boolean beenRead;
	
	public Message() {
		super();
	}
	
	public Message(Date created, String message, Boolean beenRead) {
		super();
		this.created = created;
		this.message = message;
		this.beenRead = beenRead;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date timestamp) {
		this.created = timestamp;
	}

	@Column
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column
	public Boolean getBeenRead() {
		return beenRead;
	}

	public void setBeenRead(Boolean read) {
		this.beenRead = read;
	}

}
