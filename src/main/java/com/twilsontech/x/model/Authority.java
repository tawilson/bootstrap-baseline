package com.twilsontech.x.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "AUTHORITY")
public class Authority extends BaseAbstractModel implements GrantedAuthority {

	public enum Role {

		ROLE_USER("USER"), 
		ROLE_ADMIN("ADMIN"), 	
		ROLE_API_USER("API_USER"), 
		ROLE_SYSTEM("SYSTEM");

		private String shortName;

		private Role(String shortName) {
			this.shortName = shortName;
		}

		public String getShortName() {
			return shortName;
		}
		
		public String getName() {
			return this.name();
		}

	}

	private static final long serialVersionUID = 1L;
	private String authority;
	private User user;

	public Authority(String authority, User user) {
		super();
		this.authority = authority;
		this.user = user;
	}

	public Authority() {
		super();
	}

	@Column(length = 50)
	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", nullable = false)	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Authority [id=" + id + ", authority=" + authority
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authority == null) ? 0 : authority.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Authority other = (Authority) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		return true;
	}

}
