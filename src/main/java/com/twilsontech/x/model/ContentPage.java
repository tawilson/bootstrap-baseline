package com.twilsontech.x.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "CONTENT_PAGE")
public class ContentPage extends BaseAuthAdvisable {

	public ContentPage() {
		super(ResourceType.CONTENT_PAGE);
	}

	public ContentPage(String browserTitle, String pageTitle, String content,
			String urlPath) {
		this();
		this.browserTitle = browserTitle;
		this.pageTitle = pageTitle;
		this.content = content;
		this.urlPath = urlPath;
	}

	public ContentPage(Integer id) {
		this();
		this.id = id;
	}

	private String browserTitle;
	private String pageTitle;
	private String content;
	private String urlPath;

	@Column
	public String getBrowserTitle() {
		return browserTitle;
	}

	public void setBrowserTitle(String browserTitle) {
		this.browserTitle = browserTitle;
	}

	@Column
	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	@Column
	@Lob
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column
	public String getUrlPath() {
		return urlPath;
	}

	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}

	@Override
	public String toString() {
		return "ContentPage [browserTitle=" + browserTitle + ", pageTitle="
				+ pageTitle + ", content=" + content + ", urlPath=" + urlPath
				+ "]";
	}

}
