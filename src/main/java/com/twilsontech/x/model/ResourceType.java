package com.twilsontech.x.model;

public enum ResourceType {

	USER(User.class.getName(), "int"),
	CONTENT_PAGE(ContentPage.class.getName(), "int"),
	CONTENT_PAGE_CONTEXT(ContentPageContext.class.getName(), "int"),
	MEDIA(Media.class.getName(), "int"),
	APP_PROPS(ApplicationProperty.class.getName(), "int");

	private String resourceTypeName;
	private String idType;

	private ResourceType(String resourceTypeName, String idType) {
		this.resourceTypeName = resourceTypeName;
		this.idType = idType;
	}

	public String getResourceTypeName() {
		return resourceTypeName;
	}

	public String getIdType() {
		return idType;
	}

}
