package com.twilsontech.x.model;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name = "CONTENT_MEDIA")
public class Media extends BaseAuthAdvisable {
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a"); 

	private String originalFilename;
	private String contentType;
	private long size;
	private byte[] data;
	private String fileNameOnDisk;

	public Media() {
		super(ResourceType.MEDIA);
	}
	
	public Media(Integer id) {
		super(ResourceType.MEDIA);
		setId(id);
	}

	public Media(String originalFilename, String contentType, long size,
			byte[] data) {
		this();
		this.originalFilename = originalFilename;
		this.contentType = contentType;
		this.size = size;
		this.data = data;
	}

	@Column(name = "ORIGINAL_FILE_NAME", unique = true)
	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	/** It's actually stored on disk. */
	@Transient
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@Column(length = 400)
	public String getFileNameOnDisk() {
		return fileNameOnDisk;
	}

	public void setFileNameOnDisk(String fileNameOnDisk) {
		this.fileNameOnDisk = fileNameOnDisk;
	}
	
	@Transient
	public String getFormattedOriginalFilename() {
		return StringUtils.abbreviate(this.originalFilename, 25);
	}
	
	@Transient
	public String getFormattedSize() {
		return NumberFormat.getInstance().format(size);
	}
	
	@Transient
	public String getFormattedCreateDate() {
		return dateFormat.format(createDate);
	}
		
}
