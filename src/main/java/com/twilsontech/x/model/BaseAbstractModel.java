package com.twilsontech.x.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class BaseAbstractModel implements Serializable {

	protected Integer id;
	protected Date createDate;
	protected Date lastUpdate;

	public BaseAbstractModel() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// works well with MYSQL auto increment
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Transient
	public boolean isNew() {
		return (id == null) || (id == 0);
	}

	@Column(updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	@PrePersist 
	public void onPrePersist() {
		if(isNew()) {
			createDate = new Date();
			lastUpdate = new Date();
		} else {
			lastUpdate = new Date();			
		}
	}

}
