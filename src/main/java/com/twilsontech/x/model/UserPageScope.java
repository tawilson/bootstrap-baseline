package com.twilsontech.x.model;

/**
 * The scope of the user page.  Typically the scope of the page that they are viewing.
 */
public enum UserPageScope {
	
	ALL, MAIN, AUTH;
	
	public String getName() {
		return this.name();
	}

}
