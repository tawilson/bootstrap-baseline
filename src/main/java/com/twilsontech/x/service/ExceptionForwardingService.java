package com.twilsontech.x.service;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.twilsontech.x.util.EnvironmentUtils;

@Service
public class ExceptionForwardingService {
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionForwardingService.class);	
	
	private Environment environment;
	private EmailService emailService;
	
	public void forward(Principal principal, Throwable t, String requestUri, String userEnvironment) {
		StringBuilder builder = new StringBuilder();
		builder.append(EnvironmentUtils.listActiveProfiles(environment)).append("--");
		builder.append(principal == null ? "anonymous" : principal.getName()).append(" -- ").append(t).append(" -- ").append(requestUri).append(" -- ").append(userEnvironment);
		Map<String, Object> info = new HashMap<String, Object>();
		info.put("message", builder.toString());
		emailService.sendAdminEmail("Bootstrap Baseline Exception", info);
	}
	
	/**  Exceptions where there is no user logged on. */
	public void forward(Throwable t) {
		Map<String, Object> info = new HashMap<String, Object>();
		info.put("message", buildSubjectBody(t));		
		emailService.sendAdminEmail("Bootstrap Baseline Exception", info);
	}

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}
	
	@Autowired
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	private String buildSubjectBody(Throwable t) {
		return EnvironmentUtils.listActiveProfiles(environment) + " - " + t.getClass().getName() + " - " + t.getMessage();
	}
	
}
