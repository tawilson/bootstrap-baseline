package com.twilsontech.x.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twilsontech.x.model.ApplicationProperty;
import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.auth.Authorize;
import com.twilsontech.x.repositories.jpa.ApplicationPropertiesRepository;

@Service
public class ApplicationPropertiesService {

	private ApplicationPropertiesRepository applicationPropertiesRepository;

	public ApplicationProperty findOne(Integer id) {
		return applicationPropertiesRepository.findOne(id);
	}

	public Iterable<ApplicationProperty> findAll() {
		return applicationPropertiesRepository.findAll();
	}

	public ApplicationProperty create(ApplicationProperty applicationProperty) {
		return applicationPropertiesRepository.save(applicationProperty);
	}

	@Authorize(action = DomainAction.UPDATE, resourceType = ResourceType.APP_PROPS)
	public ApplicationProperty update(ApplicationProperty applicationProperty) {
		return applicationPropertiesRepository.save(applicationProperty);
	}

	@Autowired
	public void setApplicationPropertiesRepository(
			ApplicationPropertiesRepository applicationPropertiesRepository) {
		this.applicationPropertiesRepository = applicationPropertiesRepository;
	}

}
