package com.twilsontech.x.service;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.twilsontech.x.common.exception.DefaultException;
import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.Media;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.auth.Authorize;
import com.twilsontech.x.repositories.jpa.MediaRepository;

@Service
public class MediaService {

	private static final Logger logger = LoggerFactory
			.getLogger(MediaService.class);

	private MediaRepository mediaRepository;
	private String mediaDirectory;
	private Environment environment;

	public MediaService() {
	}

	@PostConstruct
	public void init() {		
		mediaDirectory = environment.getRequiredProperty("mediaDirectory");
		if(!isMediaDirectoryWritable()) {
			logger.warn("Unable to write to media directory: {}", mediaDirectory);			
		}
	}
	
	public boolean isMediaDirectoryWritable() {
		return new File(mediaDirectory).canWrite();
	}

	@Authorize(action = DomainAction.LIST, resourceType = ResourceType.MEDIA)
	public Page<Media> findAll(Pageable pageable) {
		return mediaRepository.findAll(pageable);
	}

	@Authorize(action = DomainAction.CREATE, resourceType = ResourceType.MEDIA)
	public Media save(Media media) {

		String fileNameOnDisk = System.currentTimeMillis() + "_"
				+ media.getOriginalFilename();
		String fileName = createFullPathOnDisk(fileNameOnDisk);

		try {
			logger.debug("Writing File: " + fileName);
			FileUtils.writeByteArrayToFile(new File(fileName), media.getData());

			media.setFileNameOnDisk(fileNameOnDisk);
			return mediaRepository.save(media);

		} catch (IOException e) {
			logger.error("Unable to save file: {}", fileName, e);
			throw new DefaultException("Unable to save file: " + fileName, e);
		}

	}

	/**
	 * Used to serve the media resources.
	 * 
	 * @param originalFilename
	 * @return
	 */
	public Media findByOriginalFilename(String originalFilename) {
		return mediaRepository.findByOriginalFilename(originalFilename);
	}

	public void copyMediaToOutputStream(Media media, OutputStream outputStream) throws IOException {
		FileUtils.copyFile(
				new File(createFullPathOnDisk(media.getFileNameOnDisk())),
				outputStream);
	}
	
	@Authorize(action = DomainAction.DELETE, resourceType = ResourceType.MEDIA)
	public void delete(Media media) {		
		Media fullMedia = mediaRepository.findOne(media.getId());
		mediaRepository.delete(media.getId());		
		new File(createFullPathOnDisk(fullMedia.getFileNameOnDisk())).delete();
	}

	@Autowired
	public void setMediaRepository(MediaRepository mediaRepository) {
		this.mediaRepository = mediaRepository;
	}

	public void setMediaDirectory(String mediaDirectory) {
		this.mediaDirectory = mediaDirectory;
	}
	
	public String getMediaDirectory() {
		return mediaDirectory;
	}

	@Autowired
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	private String createFullPathOnDisk(String fileNameOnDisk) {
		return new StringBuilder().append(mediaDirectory)
				.append(File.separator).append(fileNameOnDisk).toString();
	}

}
