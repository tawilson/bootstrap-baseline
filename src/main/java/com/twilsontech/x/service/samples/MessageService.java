package com.twilsontech.x.service.samples;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.twilsontech.x.model.Message;
import com.twilsontech.x.repositories.jpa.MessageRepository;

@Service
public class MessageService {

	private MessageRepository messageRepository;

	public Page<Message> findAll(Pageable pageable) {
		return messageRepository.findAll(pageable);
	}

	public <S extends Message> S save(S message) {
		if(message.getCreated() == null) {
			message.setCreated(new Date());
		}
		return messageRepository.save(message);
	}
	
	public List<Message> findByTimestampBetween(Date from, Date to) {
		return messageRepository.findByCreatedBetween(from, to);
	}
	
	public Message findOne(Integer id) {
		return messageRepository.findOne(id);
	}
	
	public List<Message> findByMessageContaining(String query) {
		return messageRepository.findByMessageContaining(query);
	}
	
	public void delete(Message message) {
		messageRepository.delete(message);
	}
	
	public long count() {
		return messageRepository.count();
	}

	@Autowired
	public void setMessageRepository(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
	}

}
