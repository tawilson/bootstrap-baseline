package com.twilsontech.x.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.User;
import com.twilsontech.x.model.audit.AuditRecord;
import com.twilsontech.x.model.auth.AuthAdvisable;
import com.twilsontech.x.model.auth.AuthEffect;
import com.twilsontech.x.model.auth.AuthorizationContext;
import com.twilsontech.x.model.auth.Authorize;
import com.twilsontech.x.repositories.jpa.AuthorizationContextRepository;

@Aspect
@Service
public class AuthorizationService {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthorizationService.class);
	
	private UserService userService;
	private AuditRecordService auditRecordService;
	private AuthorizationContextRepository authorizationContextRepository;
	private	Sort resourceIdSort = new Sort(Direction.ASC,"resourceId");
	
	public static final String EVERYTHING = "*";

	public boolean authorize(Principal principal, Object typeKey, String type, String action) {
		return false;
	}
	
	public void save(AuthorizationContext authorizationContext) {
		authorizationContextRepository.save(authorizationContext);
	}
	
	/**
	 * TODO: Document.  Use this for "everything" or an action not associated with a particular resource.
	 * @param principal
	 * @param resourceType
	 * @param action
	 */
	public void addAuthContextNoResource(String principal, String resourceType, DomainAction action) {
		AuthorizationContext authorizationContext = new AuthorizationContext();
		authorizationContext.setAction(action);
		authorizationContext.setPrincipal(principal);
		authorizationContext.setResourceId(EVERYTHING);
		authorizationContext.setResourceType(resourceType);
		authorizationContextRepository.save(authorizationContext);
	}	
	
	public void addAuthContextForResource(String principal, String resourceType, String resourceId, DomainAction action) {
		AuthorizationContext authorizationContext = new AuthorizationContext();
		authorizationContext.setAction(action);
		authorizationContext.setPrincipal(principal);
		authorizationContext.setResourceId(resourceId);
		authorizationContext.setResourceType(resourceType);
		authorizationContextRepository.save(authorizationContext);
	}	
	
	public void addAuthContextForResource(String[] principals, String resourceType, String resourceId, AuthEffect effect, DomainAction[] actions ) {
		
		for(String principal: principals) {
			for(DomainAction domainAction : actions) {
				AuthorizationContext authorizationContext = new AuthorizationContext();
				authorizationContext.setAction(domainAction);
				authorizationContext.setPrincipal(principal);
				authorizationContext.setResourceId(resourceId);
				authorizationContext.setResourceType(resourceType);
				authorizationContextRepository.save(authorizationContext);			
			}
		}
		
	}	
	
	public Collection<Object> findAccessibleIdsForUser(User user, ResourceType resourceType, DomainAction domainAction) {
		
		// We want the everything resource id to be at the top.
		
		List<Object> allows = new ArrayList<>();
		List<AuthorizationContext> authorizationContexts = authorizationContextRepository.findByPrincipalInAndResourceTypeAndAction(user.populatePrincipals(), resourceType.getResourceTypeName(), domainAction, resourceIdSort);
		for(AuthorizationContext authorizationContext : authorizationContexts) {
			// TODO: Perhaps use a more OO way to convert ids.
			if(resourceType.getIdType().equals("int")) {
				allows.add(new Integer(authorizationContext.getResourceId()));
			}
		}
		
		logger.debug("Allowable ID Count - User: {}, ResourceType: {}, Action: {}, Count: {}", user.getUsername(), resourceType, domainAction, allows.size());		
		return allows;
	}
	
	//@Before("@annotation(authorize)")
	@Around("@annotation(authorize)")
	public Object aroundWithAuthDeclaration(ProceedingJoinPoint pjp, Authorize authorize)  throws Throwable {
		boolean allow = true;
		
		User user = userService.getAuthenticatedUser();
		
		logger.debug("Confirming Access: User: {}, ResourceType: {}, Action: {}", user, authorize.resourceType(), authorize.action());		
		
		if(authorize.action().equals(DomainAction.CREATE)) {
			// we'll save the audit records after the object has been persisted so the id is available
			allow = hasMatch(user, authorize, null);
		} else if(authorize.action().equals(DomainAction.LIST)) {
			auditRecordService.save(new AuditRecord(user, authorize.resourceType(), null, authorize.action()));
			allow = hasMatch(user, authorize, null);  			
		} else {
			for(Object arg : pjp.getArgs()) {
				if(arg instanceof AuthAdvisable) {
					AuthAdvisable authAdvisable = (AuthAdvisable) arg;					
					auditRecordService.save(new AuditRecord(user, authorize.resourceType(), authAdvisable.getAuthKey(), authorize.action()));					
					allow = hasMatch(user, authorize, authAdvisable); 
					
					if(!allow) {
						break;
					}
				}
			}			
		}
				
		if(allow) {
			Object returnObj = pjp.proceed();			
			// the objects should now have their id's populated.
			if(authorize.action().equals(DomainAction.CREATE)) {
				for(Object arg : pjp.getArgs()) {
					if(arg instanceof AuthAdvisable) {
						AuthAdvisable authAdvisable = (AuthAdvisable) arg;						
						auditRecordService.save(new AuditRecord(user, authorize.resourceType(), authAdvisable.getAuthKey(), authorize.action()));								
					}
				}				
			}
			return returnObj;
		} else {
			logger.warn("Access Denied: User: {}, ResourceType: {}, Action: {}", user, authorize.resourceType(), authorize.action());
			throw new AccessDeniedException("Access denied to resource.");
		}
		
	}

	@Autowired
	public void setAuthorizationContextRepository(AuthorizationContextRepository authorizationContextRepository) {
		this.authorizationContextRepository = authorizationContextRepository;
	}
	
	@Autowired
	public void setAuditRecordService(AuditRecordService auditRecordService) {
		this.auditRecordService = auditRecordService;
	}
	
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	private boolean hasMatch(User user, Authorize authorize, AuthAdvisable authAdvisable) {
				
		List<String> resourceIds = new ArrayList<>();
		resourceIds.add(EVERYTHING);
		if(authAdvisable != null) {
			resourceIds.add(authAdvisable.getAuthKey());
		}
		
		List<AuthorizationContext> authorizationContexts = authorizationContextRepository.findByPrincipalInAndResourceTypeAndActionAndResourceIdIn(user.populatePrincipals(), authorize.resourceType().getResourceTypeName(), authorize.action(), resourceIds, resourceIdSort);
		logger.debug("AuthorizationContext Count - User: {}, ResourceType: {}, Action: {}, Count: {}", user.getUsername(), authorize.resourceType(), authorize.action(), authorizationContexts.size());
		
		return !authorizationContexts.isEmpty();
	}
	
}
