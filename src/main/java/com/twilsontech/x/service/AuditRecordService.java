package com.twilsontech.x.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.audit.AuditRecord;
import com.twilsontech.x.repositories.jpa.AuditRecordRepository;

@Service
public class AuditRecordService {
	
	private AuditRecordRepository auditRecordRepository;
	
	/**
	 * Method call is asynchronous.  No need to slow execution.
	 */
	@Async
	public <S extends AuditRecord> S save(S auditRecord) {
		return auditRecordRepository.save(auditRecord);
	}
	
	public Page<AuditRecord> findAll(Pageable pageable) {
		return auditRecordRepository.findAll(pageable);
	}
	
	/**
	 * Used to see the user responsible for particular action.
	 */
	public AuditRecord findByResourceTypeResourceIdAction(ResourceType resourceType, String resourceId, DomainAction action) {
		return auditRecordRepository.findByResourceTypeAndResourceIdAndAction(resourceType, resourceId, action);
	}

	@Autowired
	public void setAuditRecordRepository(AuditRecordRepository auditRecordRepository) {
		this.auditRecordRepository = auditRecordRepository;
	}
	
}
