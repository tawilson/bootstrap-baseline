package com.twilsontech.x.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twilsontech.x.common.exception.DefaultException;
import com.twilsontech.x.model.ContentPageContext;
import com.twilsontech.x.repositories.jpa.ContentPageContextRepository;

@Service
public class ContentPageContextService {

	private ContentPageContextRepository contentPageContextRepository;
	
	public ContentPageContext create(ContentPageContext contentPageContext) {
		if(contentPageContextRepository.count() == 0) {
			return contentPageContextRepository.save(contentPageContext);
		} else {
			throw new DefaultException("You can only create one ContentPageContext");
		}
	}	
	
	public void save(ContentPageContext contentPageContext) {
		if(contentPageContext.getId() != null) {
			contentPageContextRepository.save(contentPageContext);
		} else {
			throw new DefaultException("You cannot create a new ContentPageContext");
		}
	}
	
	/** Return the one instance of ContentPageContext */ 
	public ContentPageContext findOne() {
		return contentPageContextRepository.findAll().iterator().next();
	}

	@Autowired
	public void setContentPageContextRepository(
			ContentPageContextRepository contentPageContextRepository) {
		this.contentPageContextRepository = contentPageContextRepository;
	}

}
