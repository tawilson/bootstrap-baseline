package com.twilsontech.x.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.twilsontech.x.model.ContentPage;
import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.auth.Authorize;
import com.twilsontech.x.repositories.jpa.ContentPageRepository;

@Service
public class ContentPageService {
	
	private ContentPageRepository contentPageRepository;
	
	public ContentPage findByUrlPath(String urlPath) {
		return contentPageRepository.findByUrlPath(urlPath);
	}
	
	@Authorize(action = DomainAction.LIST, resourceType = ResourceType.CONTENT_PAGE)
	public Page<ContentPage> findAll(Pageable pageable) {
		return contentPageRepository.findAll(pageable);
	}

	@Authorize(action = DomainAction.READ, resourceType = ResourceType.CONTENT_PAGE)
	public ContentPage findOne(ContentPage contentPage) {
		return contentPageRepository.findOne(contentPage.getId());
	}

	@Authorize(action = DomainAction.CREATE, resourceType = ResourceType.CONTENT_PAGE)
	public <S extends ContentPage> S create(S contentPage) {
		return contentPageRepository.save(contentPage);
	}
	
	@Authorize(action = DomainAction.UPDATE, resourceType = ResourceType.CONTENT_PAGE)
	public <S extends ContentPage> S update(S contentPage) {
		return contentPageRepository.save(contentPage);
	}	
	
	@Authorize(action = DomainAction.DELETE, resourceType = ResourceType.CONTENT_PAGE)
	public void delete(ContentPage contentPage) {
		contentPageRepository.delete(contentPage);
	}

	@Autowired
	public void setContentPageRepository(ContentPageRepository contentPageRepository) {
		this.contentPageRepository = contentPageRepository;
	}

}
