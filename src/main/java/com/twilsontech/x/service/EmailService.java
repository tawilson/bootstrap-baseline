package com.twilsontech.x.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.twilsontech.x.common.exception.DefaultException;

public class EmailService {

	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);	
	
	private String overrideTo;
	private JavaMailSender javaMailSender;
	private VelocityEngine velocityEngine;
	private Map<String, Object> defaultModel = new HashMap<String, Object>();	
	private String defaultFrom;
	private String replyTo;
	private String adminRecipient;
	
	/**
	 * Marked as private.  Must use the EmailServiceBuilder to construct a new instance.
	 */
	private EmailService() {
	}
	
	@Async
	public void sendAdminEmail(String subject, Map<String, Object> information) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom(defaultFrom);
		mailMessage.setReplyTo(replyTo);
		
		if(useOverrideTo()) {
			mailMessage.setTo(overrideTo);
		} else {
			mailMessage.setTo(adminRecipient);	
		}	
		
		
		StringBuilder builder = new StringBuilder();
		for(Map.Entry<String, Object> entry : information.entrySet()) {
			builder.append(entry.getKey()).append(":").append(entry.getValue()).append("\n");
		}
		
		mailMessage.setSubject(subject);
		mailMessage.setText(builder.toString());
		
		javaMailSender.send(mailMessage);
	}

	public void sendMessage(String to, String message, String subject) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom(defaultFrom);
		mailMessage.setReplyTo(replyTo);		
		
		if(useOverrideTo()) {
			mailMessage.setTo(overrideTo);
		} else {
			mailMessage.setTo(to);	
		}		
		
		mailMessage.setSubject(subject);
		mailMessage.setText(message);

		javaMailSender.send(mailMessage);
		
	}
	
	public void sendMessage(String[] to, String message, String subject) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setFrom(defaultFrom);
		mailMessage.setReplyTo(replyTo);
		
		if(useOverrideTo()) {
			mailMessage.setTo(overrideTo);
		} else {
			mailMessage.setTo(to);	
		}		

		mailMessage.setSubject(subject);
		mailMessage.setText(message);

		javaMailSender.send(mailMessage);
		
	}

	/** Use the default From Account. */
	public void sendTemplatedMessage(final String to, final String subject,
			final Map<String, Object> model, final String templateName) {		
		sendTemplatedMessage(defaultFrom, to, subject, model, templateName);
	}
	
	/** Select another from account. */
	public void sendTemplatedMessage(final String from, final String to, final String subject,
			final Map<String, Object> model, final String templateName) {
		
		model.putAll(defaultModel);
				
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setFrom(from);
				message.setReplyTo(replyTo);
				
				if(useOverrideTo()) {
					message.setTo(overrideTo);
				} else {
					message.setTo(to);	
				}								
				message.setSubject(subject);
				String text = VelocityEngineUtils.mergeTemplateIntoString(
						velocityEngine, templateName, "UTF-8",
						model);
				message.setText(text, true);
			}
		};
			
		javaMailSender.send(preparator);
		logger.debug("Message Sent to: " + to);
	}	


	private boolean useOverrideTo() {
		return StringUtils.isNotEmpty(overrideTo);
	}
	
	/**
	 * Builds an instance of an EmailService
	 */
	public static class EmailServiceBuilder {
		
		private String host;
		private int port = 25;
		private String from;
		private Map<String, Object> templateModel;
		private String replyTo;
		private String adminRecipient;
		private String overrideTo;

		public EmailServiceBuilder(String host) {
			this.host = host;
		}
		
		public EmailServiceBuilder from(String from) {
			this.from = from;
			return this;
		}

		public EmailServiceBuilder port(int port) {
			this.port = port;
			return this;
		}
		
		public EmailServiceBuilder port(String port) {
			return port(new Integer(port));
		}		
		
		public EmailServiceBuilder replyTo(String replyTo) {
			this.replyTo = replyTo;
			return this;
		}
		
		public EmailServiceBuilder addTemplateModel(Map<String, Object> templateModel) {
			this.templateModel = templateModel;
			return this;
		}
		
		public EmailServiceBuilder adminRecipient(String adminRecipient) {
			this.adminRecipient = adminRecipient;
			return this;
		}
		
		public EmailServiceBuilder overrideTo(String overrideTo) {
			this.overrideTo = overrideTo;
			return this;
		}
		
		public EmailService build() {
			Properties javaMailProperties = new Properties();
			javaMailProperties.put("mail.from", from);
			JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
			javaMailSenderImpl.setHost(host);
			javaMailSenderImpl.setPort(port);
			javaMailSenderImpl.setJavaMailProperties(javaMailProperties);

			Properties velocityProperties = new Properties();
			velocityProperties.put("resource.loader", "class");
			velocityProperties
					.put("class.resource.loader.class",
							"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			VelocityEngineFactoryBean velocityEngineFactoryBean = new VelocityEngineFactoryBean();
			velocityEngineFactoryBean.setVelocityProperties(velocityProperties);

			EmailService emailService = new EmailService();
			emailService.overrideTo = this.overrideTo;
			emailService.defaultFrom = this.from;
			emailService.replyTo = this.replyTo;
			emailService.adminRecipient = this.adminRecipient;
			if(templateModel != null) {
				emailService.defaultModel.putAll(templateModel);
			}
			emailService.javaMailSender = javaMailSenderImpl;
			try {
				emailService.velocityEngine = velocityEngineFactoryBean.createVelocityEngine();
			} catch (Exception e) {
				logger.error("Unable to create velocity engine", e);
				throw new DefaultException(e);
			} 

			return emailService;
		}
		
	}	

	
}
