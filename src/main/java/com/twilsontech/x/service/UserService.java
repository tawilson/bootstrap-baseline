package com.twilsontech.x.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.twilsontech.x.common.PasswordUtils;
import com.twilsontech.x.common.exception.DefaultException;
import com.twilsontech.x.model.Authority;
import com.twilsontech.x.model.Authority.Role;
import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.ResetPassword;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.User;
import com.twilsontech.x.model.UserTemp;
import com.twilsontech.x.model.audit.AuditRecord;
import com.twilsontech.x.model.auth.AuthEffect;
import com.twilsontech.x.model.auth.Authorize;
import com.twilsontech.x.repositories.jpa.UserRepository;
import com.twilsontech.x.repositories.jpa.UserTempRepository;

@Service
public class UserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserService.class);
	
	private static final String SEND_PASSWORD_RESET = "sendPasswordReset";	

	private AuthorizationService authorizationService;
	private UserRepository userRepository;
	private PasswordUtils passwordUtils;
	private EmailService emailService;
	private UserTempRepository userTempRepository;
	private AuditRecordService auditRecordService;

	public void registerUser(User newUser) {

		User user = new User(newUser.getPassword(),
				newUser.getUsername());

		user.getAuthorities().add(
				new Authority(Role.ROLE_USER.name(), user));
		persistNewUser(user);
		
		auditRecordService.save(new AuditRecord(user, ResourceType.USER, user.getAuthKey(), DomainAction.CREATE));		
		
		emailService.sendTemplatedMessage(user.getUsername(), "Thank Your For Joining", new HashMap<String, Object>(), "template/registerSuccess.html");
		
		Map<String, Object> info = new HashMap<>();
		info.put("Registration Time", new DateTime());
		info.put("User Info", user);
		emailService.sendAdminEmail("New User Registration: " + user.getUsername(), info);
	}
	
	/** No ACL check or auditing.  Used for creating system users. */
	public User createSystemUser(User user) {
		user.setPassword(passwordUtils.getbCryptPasswordEncoder().encode(
				user.getPassword()));	
		
		User systemUser = userRepository.save(user);
		
		authorizationService.addAuthContextForResource(
				new String[]{systemUser.getUsername(), Role.ROLE_ADMIN.name()}, 
				ResourceType.USER.getResourceTypeName(), 
				new Integer(user.getId()).toString(), 
				AuthEffect.ALLOW, 
				new DomainAction[]{DomainAction.READ});		
		
		return systemUser;
	}

	/** Typically used by admin and system services. */
	@Authorize(action = DomainAction.CREATE, resourceType = ResourceType.USER)
	public User createUser(User newUser, boolean sendEmail) {
		User user = persistNewUser(newUser);
		
		if(sendEmail) {
			sendPasswordReset(newUser.getUsername());
		}
		
		return user;
	}	
	
	@Authorize(action = DomainAction.UPDATE, resourceType = ResourceType.USER)	
	public User update(User user) {
		return userRepository.save(user);
	}	

	/** Use this method if you do not want or need audit records saved. */
	public User findOne(Integer userId) {
		return userRepository.findOne(userId);
	}
	
	/**
	 * User this method to audit its use.
	 * @param user
	 * @return
	 */
	@Authorize(action = DomainAction.READ, resourceType = ResourceType.USER)	
	public User find(User user) {
		return userRepository.findOne(user.getId());
	}	
	
	@Authorize(action = DomainAction.LIST, resourceType = ResourceType.USER)	
	public Page<User> findAll(Pageable pageable) {
		return userRepository.findAll(pageable);
	}
	
	@Authorize(action = DomainAction.LIST, resourceType = ResourceType.USER)	
	public Page<User> findByDeletedAndUsernameNot(boolean deleted, String username, Pageable pageable) {
		return userRepository.findByDeletedAndUsernameNot(deleted, username, pageable);
	}

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		User user = userRepository.findByUsername(username);
		if (user != null) {
			logger.debug(user.toString());
			return user;
		} else {
			throw new UsernameNotFoundException(username + " not found");
		}

	}
	
	public boolean isCorrectPassword(String username, String password) {
		User user = userRepository.findByUsername(username);
		return passwordUtils.getbCryptPasswordEncoder().matches(password, user.getPassword());
	}
	
	public void updatePassword(ResetPassword resetPassword) {
		User user = userRepository.findByUsername(resetPassword.getUsername());
		savePassword(user, resetPassword);
	}
	
	public void sendPasswordReset(String username) {
		
		User user = userRepository.findByUsername(username);				
		
		// One may already exist.  If so re-use it.
		UserTemp userTemp = userTempRepository.findByUserIdAndKey(user.getId(), SEND_PASSWORD_RESET);
		if(userTemp == null) {			
			userTemp = new UserTemp();
			userTemp.setKey(SEND_PASSWORD_RESET);
			userTemp.setUserId(user.getId());
			userTemp.setValue(RandomStringUtils.randomAlphanumeric(40));
			userTempRepository.save(userTemp);			
		}
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("e", username);
		model.put("token", userTemp.getValue());
		
		emailService.sendTemplatedMessage(username, "Reset Password", model, "template/resetPassword.html");
	}	
	
	public User resetPasswordValidate(String email, String token) {
		
		User foundUser = null;
		User user = userRepository.findByUsername(email);
		
		if((user != null)) {		
			UserTemp userTemp = userTempRepository.findByUserIdAndKey(user.getId(), SEND_PASSWORD_RESET);
			if(userTemp.getValue().equals(token)) {				
				foundUser = user;
			}
		}
		return foundUser;
	}	
	
	public void externalResetPassword(ResetPassword resetPassword) {
		User user = userRepository.findByUsername(resetPassword.getUsername());
		UserTemp userTemp = userTempRepository.findByUserIdAndKey(user.getId(), SEND_PASSWORD_RESET);
		
		if(userTemp.getValue().equals(resetPassword.getAuthToken())) {
			savePassword(user, resetPassword);
			userTempRepository.delete(userTemp);
		}
	}

	public Iterable<User> findAll() {
		return userRepository.findAll();
	}
	
	public User getAuthenticatedUser() {
		User user = null;
		try {
			user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (ClassCastException e) {
			throw new DefaultException("User not authenticated.");
		}
		return user;
	}
	
	public boolean isAuthenticated(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    return authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
	}	
	
	
	@Authorize(action = DomainAction.DELETE, resourceType = ResourceType.USER)		
	public void deleteUser(User user) {
		user = userRepository.findOne(user.getId());
		user.setDeleted(true);
		user.setEnabled(false); // setting enabled to false prevents the user from logging in
		userRepository.save(user);
	}
	
	@Authorize(action = DomainAction.UN_DELETE, resourceType = ResourceType.USER)		
	public void unDeleteUser(User user) {
		user = userRepository.findOne(user.getId());
		user.setDeleted(false);
		user.setEnabled(true);
		userRepository.save(user);
	}	

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setPasswordUtils(PasswordUtils passwordUtils) {
		this.passwordUtils = passwordUtils;
	}

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setUserTempRepository(UserTempRepository userTempRepository) {
		this.userTempRepository = userTempRepository;
	}
	
	@Autowired
	public void setAuthorizationService(AuthorizationService authorizationService) {
		this.authorizationService = authorizationService;
	}
	
	@Autowired
	public void setAuditRecordService(AuditRecordService auditRecordService) {
		this.auditRecordService = auditRecordService;
	}

	private void savePassword(User user, ResetPassword resetPassword) {
		user.setPassword(passwordUtils.getbCryptPasswordEncoder().encode(
				resetPassword.getPassword()));	
		userRepository.save(user);		
	}
	
	private User persistNewUser(User newUser) {
		newUser.setPassword(passwordUtils.getbCryptPasswordEncoder().encode(
				newUser.getPassword()));
		
		User user = userRepository.save(newUser);
		
		authorizationService.addAuthContextForResource(
				new String[]{user.getUsername(), Role.ROLE_ADMIN.name()}, 
				ResourceType.USER.getResourceTypeName(), 
				new Integer(user.getId()).toString(), 
				AuthEffect.ALLOW, 
				new DomainAction[]{DomainAction.READ, DomainAction.UPDATE, DomainAction.DELETE, DomainAction.UN_DELETE});
		
		return user;
	}	
	
}
