package com.twilsontech.x.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.joda.time.DateTime;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.ShallowEtagHeaderFilter;
import org.springframework.web.servlet.DispatcherServlet;

import com.opensymphony.sitemesh.webapp.SiteMeshFilter;
import com.twilsontech.x.util.EnvironmentUtils;

public class WebAppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		WebApplicationContext context = getContext();
		
		servletContext.setAttribute("copyrightYear", new DateTime().getYear());
		servletContext.setAttribute("initialActiveProfiles", EnvironmentUtils.listActiveProfiles(context.getEnvironment()));
		
		servletContext.addListener(new ContextLoaderListener(context));
		servletContext.addListener(new ApplicationPropertiesInitializer());
		servletContext.addListener(new DemoAppInitializer());
		servletContext.addListener(new PropertyFileReaderListener());
		
	    servletContext.addFilter("springSecurityFilterChain", new DelegatingFilterProxy()).addMappingForUrlPatterns(null, false, "/*");
		
		servletContext.addFilter("shallowEtagHeaderFilter", new ShallowEtagHeaderFilter()).addMappingForUrlPatterns(null, false, "/*");		
		
		servletContext.addFilter("clearSitemeshAppliedOnceFilter", new ClearSitemeshAppliedOnceFilter()).addMappingForUrlPatterns(
				EnumSet.of(DispatcherType.FORWARD, DispatcherType.ERROR), false, "/WEB-INF/jsp/errors/*");
		
		servletContext.addFilter("sitemeshFilter", new SiteMeshFilter()).addMappingForUrlPatterns(
				EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ERROR), false, "/*");
		
		ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
		dispatcherServlet.setLoadOnStartup(1);
		dispatcherServlet.addMapping("/");				
		
	}

	private AnnotationConfigWebApplicationContext getContext() {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(WebConfig.class);
		context.setConfigLocation("com.twilsontech.x.config");
		return context;
	}

}
