package com.twilsontech.x.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class PropertyFileReaderListener implements ServletContextListener {

	private Environment environment;

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		WebApplicationContextUtils.getRequiredWebApplicationContext(sce.getServletContext()).getAutowireCapableBeanFactory().autowireBean(this);
		
		sce.getServletContext().setAttribute("screenInactiveSeconds", environment.getProperty("screenInactiveSeconds"));
		sce.getServletContext().setAttribute("respondToDialogSeconds", environment.getProperty("respondToDialogSeconds"));

	}

	@Autowired
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

}
