package com.twilsontech.x.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClearSitemeshAppliedOnceFilter implements Filter {

	private static final Logger logger = LoggerFactory
			.getLogger(ClearSitemeshAppliedOnceFilter.class);

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain fc) throws IOException, ServletException {

		if (request.getAttribute("com.opensymphony.sitemesh.APPLIED_ONCE") != null) {
			request.removeAttribute("com.opensymphony.sitemesh.APPLIED_ONCE");
		}

		fc.doFilter(request, response);

	}

	public void init(FilterConfig fc) throws ServletException {
	}

	public void destroy() {
	}

}
