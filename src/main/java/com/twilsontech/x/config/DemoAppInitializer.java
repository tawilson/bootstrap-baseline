package com.twilsontech.x.config;

import java.util.Arrays;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.dumbster.smtp.SimpleSmtpServer;

public class DemoAppInitializer implements ServletContextListener {
	
	private static final Logger logger = LoggerFactory.getLogger(DemoAppInitializer.class);

	private DatabaseInitializer databaseInitializer;
	private Environment environment;

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		WebApplicationContextUtils
				.getRequiredWebApplicationContext(sce.getServletContext())
				.getAutowireCapableBeanFactory().autowireBean(this);

		if (Arrays.asList(environment.getActiveProfiles()).contains("demo")) {
			logger.debug("Initializing demo web components.");
			
			SimpleSmtpServer.start(new Integer(environment.getRequiredProperty("smtpPort")));
			
			databaseInitializer.init();
		}

	}

	@Autowired
	public void setDatabaseInitializer(DatabaseInitializer databaseInitializer) {
		this.databaseInitializer = databaseInitializer;
	}

	@Autowired
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

}
