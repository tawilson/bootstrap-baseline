package com.twilsontech.x.config.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:dev/application.properties")
@Profile("dev")
public class DevProfile extends BaseProfile {	
	
	private static final Logger logger = LoggerFactory.getLogger(DevProfile.class);
	
	public DevProfile() {

	}

	
    
}
