package com.twilsontech.x.config.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:local/application.properties")
@Profile("local")
public class LocalProfile extends BaseProfile {

	private static final Logger logger = LoggerFactory.getLogger(LocalProfile.class);

	public LocalProfile() {

	}

}
