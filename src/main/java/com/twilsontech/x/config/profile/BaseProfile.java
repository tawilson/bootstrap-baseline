package com.twilsontech.x.config.profile;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.twilsontech.x.service.EmailService;
import com.twilsontech.x.service.EmailService.EmailServiceBuilder;

@Configuration
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("com.twilsontech.x.repositories")
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableScheduling
public abstract class BaseProfile {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseProfile.class);
	
	protected Map<String, Object> params;
	protected MessageSource messageSource;
	protected Environment environment;
	
	public BaseProfile() {				
		// Opened another copy because it cannot be autowired due to a race condition
    	ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
    	resourceBundleMessageSource.setBasename("messages");
    	messageSource = resourceBundleMessageSource;		
	}
	
	@Bean
	public EmailService emailService() {
		logger.debug("Initializing Email Service");

		Map<String, Object> defaultModel = new HashMap<>();
		defaultModel.put("webHostname", environment.getRequiredProperty("webHostname"));	
		defaultModel.put("appName", messageSource.getMessage("appName", null, Locale.getDefault()));
		
		EmailServiceBuilder builder = new EmailServiceBuilder(environment.getProperty("smtpHost"));
		builder.port(environment.getProperty("smtpPort"));
		builder.from("\""+ messageSource.getMessage("appName", null, Locale.getDefault()) + "\" <" + environment.getRequiredProperty("default.email.address") +">");
		builder.replyTo("\"Please DO NOT REPLY\" <" + environment.getRequiredProperty("default.email.address") +">");
		builder.adminRecipient(environment.getRequiredProperty("default.email.address"));
		builder.addTemplateModel(defaultModel);
		
		String emailOverrideTo = environment.getProperty("emailOverrideTo");
		if(StringUtils.isNotEmpty(emailOverrideTo)) {
			builder.overrideTo(emailOverrideTo);
		}
		
		return builder.build();
	}
	
	@Bean
	public DataSource dataSource() {
		logger.debug("Initializing Database Connection");
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("dataSource.driverClassName"));
		dataSource.setUrl(environment.getRequiredProperty("dataSource.url"));
		dataSource.setUsername(environment.getRequiredProperty("dataSource.username"));
		dataSource.setPassword(environment.getRequiredProperty("dataSource.password"));
		
		if(creatingDatabase()) {
			logger.debug("Dropping existing tables.");
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);			
			dropTable(jdbcTemplate, "AUDIT_RECORD", "AUTH_CONTEXT", "AUTHORITY", "CONTENT_PAGE_CONTEXT", "MESSAGES", "USERS", "USER_TMP", "CONTENT_PAGE");
		}

		return dataSource;
	}
	
	@Bean
	public EntityManagerFactory entityManagerFactory() {

		Properties properties = new Properties();
		if(creatingDatabase()) {
			logger.info("Creating a new instance of the database.");
			properties.setProperty("hibernate.hbm2ddl.auto", "create");
		}
		properties.setProperty("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(new Boolean(environment.getRequiredProperty("vendorAdapter.generateDdl")));

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.twilsontech.x");
		factory.setDataSource(dataSource());
		factory.setJpaProperties(properties);
		factory.afterPropertiesSet();

		return factory.getObject();
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
		return txManager;
	}	

	@Autowired
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	private boolean creatingDatabase() {
		return Arrays.asList(environment.getActiveProfiles()).contains("create-database");
	}
	
	private void dropTable(JdbcTemplate jdbcTemplate, String... tableNames) {
		for(String tableName : tableNames) {
			try {
				jdbcTemplate.execute("DROP TABLE " + tableName);
			} catch (BadSqlGrammarException e) {
				// An exception my be thrown the first time this script is run.
				logger.warn("Unable to drop table: {}", tableName);
			}
		}
	}
	
}