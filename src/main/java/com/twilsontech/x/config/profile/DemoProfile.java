package com.twilsontech.x.config.profile;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:demo/application.properties")
@Profile("demo")
public class DemoProfile extends BaseProfile {

	public DemoProfile() {

	}

}
