package com.twilsontech.x.config;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.twilsontech.x.config.profile.DevProfile;
import com.twilsontech.x.config.profile.LocalProfile;
import com.twilsontech.x.config.profile.ProdProfile;
import com.twilsontech.x.config.profile.TestProfile;
import com.twilsontech.x.model.ApplicationProperty;
import com.twilsontech.x.model.Authority;
import com.twilsontech.x.model.Authority.Role;
import com.twilsontech.x.model.ContentPage;
import com.twilsontech.x.model.ContentPageContext;
import com.twilsontech.x.model.DomainAction;
import com.twilsontech.x.model.Message;
import com.twilsontech.x.model.ResourceType;
import com.twilsontech.x.model.User;
import com.twilsontech.x.model.auth.AuthEffect;
import com.twilsontech.x.service.ApplicationPropertiesService;
import com.twilsontech.x.service.AuthorizationService;
import com.twilsontech.x.service.ContentPageContextService;
import com.twilsontech.x.service.ContentPageService;
import com.twilsontech.x.service.UserService;
import com.twilsontech.x.service.samples.MessageService;

@Service
public class DatabaseInitializer {

	private static final Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);

	private static long rangebegin = new DateTime().minusMonths(1).getMillis();
	private static long rangeend = new DateTime().plusMonths(6).getMillis();

	private AuthorizationService authorizationService;
	private UserService userService;
	private MessageService messageService;
	private ContentPageService contentPageService;
	private ContentPageContextService contentPageContextService;
	private ApplicationPropertiesService applicationPropertiesService;
	
	public static void main(String[] args) {
		
		try {
			AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();	
			ctx.register(LocalProfile.class, DevProfile.class, TestProfile.class, ProdProfile.class, DatabaseInitializer.class);
			ctx.scan("com.twilsontech.x.service", "com.twilsontech.x.common");
			ctx.refresh();
			DatabaseInitializer databaseInitializer = ctx.getBean(DatabaseInitializer.class);
			databaseInitializer.init();
			ctx.destroy();
		} catch (Throwable e) {
			logger.error("Serious Error", e);
		} finally {
			logger.info("Initialization Complete");
			System.exit(0);
		}
	}
	
	public void init() {
		logger.info("Initializing The Database");
		// Adding Samples Messages
		for (int i = 0; i < 205; i++) {
			messageService.save(new Message(makeDate().toDate(), "test " + i,
					(i % 2 == 0)));
		}
		
		initUsers();		
		initContentPages();
		
		/* Initialize the application properties. */
		authorizationService.addAuthContextForResource(
				new String[]{Role.ROLE_ADMIN.name(), Role.ROLE_SYSTEM.name()}, 
				ResourceType.APP_PROPS.getResourceTypeName(), 
				AuthorizationService.EVERYTHING, 
				AuthEffect.ALLOW, 
				new DomainAction[]{DomainAction.UPDATE});
		applicationPropertiesService.create(new ApplicationProperty("allowUserRegistrations", "true", Boolean.class.toString()));
	}
	
	private void initUsers() {
		// Adding sample users
		String randomString = RandomStringUtils.random(10);
		User systemUser = new User(randomString, User.SYSTEM_USERNAME);
		systemUser.setAccountNonLocked(false); // so people can't login using this account
		systemUser.getAuthorities().add(
				new Authority(Role.ROLE_SYSTEM.name(), systemUser));
		systemUser = userService.createSystemUser(systemUser);	
		authorizationService.addAuthContextNoResource(Role.ROLE_SYSTEM.name(), ResourceType.USER.getResourceTypeName(), DomainAction.CREATE);
		
		// Set for logging purposes
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(systemUser, randomString));
		
		User blowUser = new User("blowblow", "joe@bootstrap-baseline.com");
		blowUser.getAuthorities().add(
				new Authority(Role.ROLE_USER.name(), blowUser));
		userService.createUser(blowUser, false);

		User adminUser = new User("blowblow", "admin@bootstrap-baseline.com");
		adminUser.getAuthorities().add(
				new Authority(Role.ROLE_USER.name(), adminUser));
		adminUser.getAuthorities().add(
				new Authority(Role.ROLE_ADMIN.name(), adminUser));
		userService.createUser(adminUser, false);	

		User remoteUser = new User("blowblow", "remote@bootstrap-baseline.com");
		remoteUser.getAuthorities().add(
				new Authority(Role.ROLE_API_USER.name(), remoteUser));
		userService.createUser(remoteUser, false);
		
		// Admin can list users
		authorizationService.addAuthContextNoResource(Role.ROLE_ADMIN.name(), ResourceType.USER.getResourceTypeName(), DomainAction.LIST);
		authorizationService.addAuthContextNoResource(Role.ROLE_ADMIN.name(), ResourceType.USER.getResourceTypeName(), DomainAction.CREATE);
	}
	
	private void initContentPages() {
		
		// Admin can do everything to any page
		authorizationService.addAuthContextForResource(
				new String[]{Role.ROLE_ADMIN.name(), Role.ROLE_SYSTEM.name()}, 
				ResourceType.CONTENT_PAGE.getResourceTypeName(), 
				AuthorizationService.EVERYTHING, 
				AuthEffect.ALLOW, 
				new DomainAction[]{DomainAction.CREATE, DomainAction.READ, DomainAction.UPDATE, DomainAction.DELETE, DomainAction.LIST});
		
		// Admin can do everything to any media object
		authorizationService.addAuthContextForResource(
				new String[]{Role.ROLE_ADMIN.name(), Role.ROLE_SYSTEM.name()}, 
				ResourceType.MEDIA.getResourceTypeName(), 
				AuthorizationService.EVERYTHING, 
				AuthEffect.ALLOW, 
				new DomainAction[]{DomainAction.CREATE, DomainAction.READ, DomainAction.UPDATE, DomainAction.DELETE, DomainAction.LIST});		
		
		/* Initialize the ContextPages */
		ContentPage indexPage = new ContentPage();
		indexPage.setBrowserTitle("index");
		indexPage.setPageTitle("index");
		indexPage.setUrlPath("index");
		indexPage.setContent("<div class=\"jumbotron\"><div class=\"container\">Hello!  Check out our Application.</div></div>");
		contentPageService.create(indexPage);
		
		ContentPage aboutPage = new ContentPage();
		aboutPage.setBrowserTitle("About Us");
		aboutPage.setPageTitle("About Us");
		aboutPage.setUrlPath("about");
		aboutPage.setContent("About Us");
		contentPageService.create(aboutPage);
		
		ContentPage contactPage = new ContentPage("Contact", "Contact", "Contact US", "contact");
		contentPageService.create(contactPage);
		
		ContentPage helpPage = new ContentPage("Help", "Help", "help", "help");
		contentPageService.create(helpPage);
		
		ContentPage termsPage = new ContentPage("Terms Of Use", "Terms Of Use", "Terms", "terms");
		contentPageService.create(termsPage);
		
		ContentPage privacyPage = new ContentPage("Privacy Policy", "Privacy Policy", "Privacy", "privacy");
		contentPageService.create(privacyPage);
		
		ContentPageContext contentPageContext = new ContentPageContext();
		contentPageContext.setIndexPage(indexPage);
		contentPageContextService.create(contentPageContext);		
	}	

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}
	
	@Autowired
	public void setAuthorizationService(AuthorizationService authorizationService) {
		this.authorizationService = authorizationService;
	}
	
	@Autowired
	public void setContentPageService(ContentPageService contentPageService) {
		this.contentPageService = contentPageService;
	}

	@Autowired
	public void setContentPageContextService(
			ContentPageContextService contentPageContextService) {
		this.contentPageContextService = contentPageContextService;
	}
	
	@Autowired
	public void setApplicationPropertiesService(
			ApplicationPropertiesService applicationPropertiesService) {
		this.applicationPropertiesService = applicationPropertiesService;
	}

	private DateTime makeDate() {
		long randomTimestamp = new RandomDataGenerator().nextLong(rangebegin,
				rangeend);
		return new DateTime(randomTimestamp);
	}

}
