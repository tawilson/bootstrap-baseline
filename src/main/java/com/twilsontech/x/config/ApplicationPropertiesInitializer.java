package com.twilsontech.x.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.twilsontech.x.model.ApplicationProperty;
import com.twilsontech.x.service.ApplicationPropertiesService;

public class ApplicationPropertiesInitializer implements ServletContextListener {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationPropertiesInitializer.class);
	
	private ApplicationPropertiesService applicationPropertiesService;

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		logger.debug("Initializing Application Properties.");
		
		WebApplicationContextUtils
				.getRequiredWebApplicationContext(sce.getServletContext())
				.getAutowireCapableBeanFactory().autowireBean(this);
		
		for(ApplicationProperty applicationProperty : applicationPropertiesService.findAll()) {
			sce.getServletContext().setAttribute(applicationProperty.getKey(), applicationProperty.getValue());
		}	

	}

	@Autowired
	public void setApplicationPropertiesService(
			ApplicationPropertiesService applicationPropertiesService) {
		this.applicationPropertiesService = applicationPropertiesService;
	}

}
