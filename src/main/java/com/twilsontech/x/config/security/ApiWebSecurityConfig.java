package com.twilsontech.x.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.twilsontech.x.model.Authority.Role;

@Configuration
@EnableWebSecurity
@Order(1)
public class ApiWebSecurityConfig extends BaseWebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
        	.antMatcher("/api/**")
			.authorizeRequests()
				.antMatchers("/api/**").hasRole(Role.ROLE_API_USER.getShortName())
				.and()
			.httpBasic()
				.and()
			.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	
}
