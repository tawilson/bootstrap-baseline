package com.twilsontech.x.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.twilsontech.x.model.Authority.Role;

@Configuration
@EnableWebSecurity
@Order(2)
public class FormWebSecurityConfig extends BaseWebSecurityConfigurerAdapter {
	
	private static final String REMEMBER_ME_KEY_NAME = "bootstrap-baseline-remember-me";
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web
			.ignoring()
				.antMatchers("/resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/auth/admin/**").hasRole(Role.ROLE_ADMIN.getShortName())			
				.antMatchers("/auth/**").hasRole(Role.ROLE_USER.getShortName())	
				.antMatchers("/**").permitAll()
				.and()
			.formLogin()
				.loginPage("/login")
				.failureUrl("/login?login_error=true")
				.defaultSuccessUrl("/auth/home")
				.permitAll()
				.and()
			.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutUrl("/logout")
				//.logoutSuccessUrl("/logout")
				.permitAll()
				.and()
			.csrf()
				.and()
			.rememberMe()
				.userDetailsService(super.userDetailsService())
				.key(REMEMBER_ME_KEY_NAME);
	}
		
}
