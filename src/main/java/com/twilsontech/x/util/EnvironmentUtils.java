package com.twilsontech.x.util;

import java.util.Arrays;

import org.springframework.core.env.Environment;

public class EnvironmentUtils {
	
	public static String listActiveProfiles(Environment environment) {
		return Arrays.toString(environment.getActiveProfiles());
	}

}
