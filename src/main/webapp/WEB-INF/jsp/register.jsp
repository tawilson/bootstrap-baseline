<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Sign Up</title>
</head>
<body>

	<div class="row large-vert-margin">	
	
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				    <div class="pull-left"><h3 class="panel-title">Please Sign Up</h3></div>
				    <div class="pull-right">or <a href="<c:url value="login" />">Sign In</a></div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal" action="<c:url value="/register" />" method="post" role="form">
					    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				
	                    <tags:formGroupTextInput type="email" label="Email Address" propertyName="username" maxlength="400" path="user.username" propertyId="username" required="true" labelWidth="col-sm-3" inputWidth="col-sm-6" />
	                    <tags:formGroupTextInput type="password" label="Password" propertyName="password" maxlength="12" path="user.password" propertyId="password" required="true" labelWidth="col-sm-3" inputWidth="col-sm-6" autocomplete="off"/>				
				
						<div class="form-group">
						   <div class="col-sm-offset-3 col-sm-6">				
							<button type="submit" class="btn btn-primary btn-block">Create My Account</button>
						   </div>
						</div>			
					</form>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>