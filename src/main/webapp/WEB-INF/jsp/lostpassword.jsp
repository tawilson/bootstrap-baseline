<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Lost Password</title>
</head>
<body>

	<div class="row large-vert-margin">	
	
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				    <div class="pull-left"><h3 class="panel-title">Lost Your Password?</h3></div>				   
				</div>
				<div class="panel-body">
				    <div class="row">
					    <div class="col-sm-4">
					    An email will be sent to you with instructions on how to reset your password.
					    </div>
					    				    				    
					    <div class="col-sm-8">
		                    <form class="form-horizontal" action="<c:url value="lostpassword" />" method="post" role="form">
		                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>	                
		                        <tags:formGroupTextInput type="email" label="Email Address" propertyName="username" maxlength="400" path="lostPassword.username" propertyId="username" required="true" labelWidth="col-sm-4" inputWidth="col-sm-7" />                                   	                
		                        <div class="form-group">
		                           <div class="col-sm-offset-4 col-sm-7">               
		                            <button type="submit" class="btn btn-primary btn-block">Send Instructions</button>
		                           </div>
		                        </div>          
		                    </form>				    
					    </div>
				    </div>
				    
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>