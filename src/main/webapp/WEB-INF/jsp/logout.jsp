<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><spring:message code="appName" /></title>
</head>
<body>

	<div class="row large-vert-margin">

		<div class="col-md-8 col-md-offset-1">

			<div class="panel panel-default">

				<div class="panel-heading clearfix">
					<div class="pull-left">
						<h3 class="panel-title">Logged Out</h3>
					</div>
				</div>
				<div class="panel-body">

					<p class="large-vert-margin">
						Either you have logged out of
						<spring:message code="appName" />
						, or your session ended after a period of inactivity. To protect your privacy we recommend
						that you log out when you are finished using
						<spring:message code="appName" />.
					</p>

					<a class="btn btn-primary btn-block" href="<c:url value="/login"/>">Login</a>

				</div>
			</div>
		</div>
	</div>

</body>
</html>