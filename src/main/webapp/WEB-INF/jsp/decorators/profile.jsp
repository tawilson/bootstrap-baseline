<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<page:apply-decorator name="main">
<!DOCTYPE html>
<html>
<head>
<title>Profile</title>
	<decorator:head /> <% // Allows decorated pages to override what's in the head. %>
    <c:set var="leftMenuSelection">
    	<decorator:getProperty property="page.leftMenuSelection" />
    </c:set>
</head>
<body>
	<a href="<c:url value="/auth/profile/" />"><tags:pageTitle title="Profile" /></a>
	
	<div class="row">
		<div  class="col-md-2">
            <div class="list-group">
                <a href="<c:url value="/auth/profile/password" />" class="list-group-item <tags:setActiveMenuItem menuItem="password" selection="${leftMenuSelection}" />">Change Password</a>
            </div>  			
		</div>
		<div class="col-md-10"><decorator:body /></div>
	</div>
</body>
</html>
</page:apply-decorator>