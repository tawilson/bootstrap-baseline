<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<page:apply-decorator name="main">
<!DOCTYPE html>
<html>
<head>
<title>Admin</title>
	<decorator:head /> <% // Allows decorated pages to override what's in the head. %>
    <c:set var="leftMenuSelection">
    	<decorator:getProperty property="page.leftMenuSelection" />
    </c:set>
</head>
<body>
	<a href="<c:url value="/auth/admin/index" />"><tags:pageTitle title="Admin" /></a>
	
	<div class="row">
		<div  class="col-md-2">
            <div class="list-group">
                <a href="<c:url value="/auth/admin/audit" />" class="list-group-item <tags:setActiveMenuItem menuItem="audit" selection="${leftMenuSelection}" />">Audit Records</a>
                <a href="<c:url value="/auth/admin/users" />" class="list-group-item <tags:setActiveMenuItem menuItem="users" selection="${leftMenuSelection}" />">Users</a>
                <a href="<c:url value="/auth/admin/pages" />" class="list-group-item <tags:setActiveMenuItem menuItem="pages" selection="${leftMenuSelection}" />">Content Pages</a>    
                <a href="<c:url value="/auth/admin/media" />" class="list-group-item <tags:setActiveMenuItem menuItem="media" selection="${leftMenuSelection}" />">Media</a>                                
                <a href="<c:url value="/auth/admin/appProps" />" class="list-group-item <tags:setActiveMenuItem menuItem="appProps" selection="${leftMenuSelection}" />">Application Props</a>                
                <a href="<c:url value="/auth/admin/systemMessage/view" />" class="list-group-item <tags:setActiveMenuItem menuItem="systemMessage" selection="${leftMenuSelection}" />">System Message</a>                                
            </div>  			
		</div>
		<div class="col-md-10"><decorator:body /></div>
	</div>
</body>
</html>
</page:apply-decorator>