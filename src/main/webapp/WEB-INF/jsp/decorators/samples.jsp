<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<page:apply-decorator name="main">
<!DOCTYPE html>
<html>
<head>
<title>Samples</title>
    <decorator:head /> <% // Allows decorated pages to override what's in the head. %>
    <c:set var="leftMenuSelection">
        <decorator:getProperty property="page.leftMenuSelection" />
    </c:set>
</head>
<body>
    <a href="<c:url value="/samples/" />"><tags:pageTitle title="Samples" /></a>
    
    <div class="row">
        <div  class="col-md-2">
            <div class="list-group">                                           
                  <a href="<c:url value="/samples/displayTag" />" class="list-group-item <tags:setActiveMenuItem menuItem="displayTag" selection="${leftMenuSelection}" />">Display Tag</a>
                  <a href="<c:url value="/samples/fullCalendar" />" class="list-group-item <tags:setActiveMenuItem menuItem="fullCalendar" selection="${leftMenuSelection}" />">Full Calendar</a>
                  <a href="<c:url value="/samples/message/" />" class="list-group-item <tags:setActiveMenuItem menuItem="html5form" selection="${leftMenuSelection}" />">HTML5 Forms</a>
                  <a href="<c:url value="/samples/tokeninput/" />" class="list-group-item <tags:setActiveMenuItem menuItem="tokeninput" selection="${leftMenuSelection}" />">Token Input</a>
                  <a href="<c:url value="/samples/confirmDialog/" />" class="list-group-item <tags:setActiveMenuItem menuItem="confirmDialog" selection="${leftMenuSelection}" />">Confirm Dialog</a>              
            </div>              
        </div>
        <div class="col-md-10"><decorator:body /></div>
    </div>
</body>
</html>
</page:apply-decorator>