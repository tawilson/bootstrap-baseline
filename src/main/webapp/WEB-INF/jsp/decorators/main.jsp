<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
  <head>
    <title><decorator:title /> | <spring:message code="appName" /></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<spring:message code="appDescription" />">
    <meta name="keywords" content="<spring:message code="appKeywords" />">    
    
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/styles.css" />" rel="stylesheet">  
    
    <script src="<c:url value="/resources/js/jquery-2.0.3.min.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>    
    
	<script src="<c:url value="/resources/js/common.js" />"></script>        

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="<c:url value="/resources/js/html5shiv.js" />"></script>
      <script src="<c:url value="/resources/js/respond.min.js" />"></script>
    <![endif]-->
	
    <decorator:head /> <% // Allows decorated pages to override what's in the head. %>
    <c:set var="mainSelection">
    	<decorator:getProperty property="page.mainSelection" />
    </c:set>
  </head>
  <body <decorator:getProperty property="body.onload" writeEntireProperty="true" />>
  
     <!-- Wrap all page content here -->
    <div id="wrap">
      <% // Default home link %>
      <c:set var="homeUrl"><c:url value="/" /></c:set> 
      <% // Menu options for authorized users. %>
      <security:authorize  access="hasRole('ROLE_USER')">
        <c:set var="homeUrl"><c:url value="/auth/" /></c:set>
        <c:set var="topNavStyles">navbar-inverse</c:set>
        <c:set var="topNavButtons">
            <li class=""><a href="<c:url value="/" />">Menu Option 1</a></li>         
        </c:set>
        <c:set var="topNavPullDown">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i> <security:authentication property="principal.username"/></a>
                <ul class="dropdown-menu">
                  <li><a href="<c:url value="/auth/profile/" />">Profile</a></li>
                  <security:authorize  access="hasRole('ROLE_ADMIN')"><li><a href="<c:url value="/auth/admin/index" />">Admin</a></li></security:authorize>
                  <li><a href="#" data-toggle="modal" data-target="#aboutDialog">About</a></li>
                  <li class="divider"></li>
                  <li><a href="<c:url value="/logout" />">Logout</a></li>
                </ul>
              </li>        
        </c:set>
      </security:authorize>
     
      <% // Menu options for unauthorized users. %>     
     <security:authorize access="isAnonymous()">
        <c:set var="homeUrl"><c:url value="/" /></c:set>     
        <c:set var="topNavStyles"></c:set>
        <c:set var="topNavButtons">
              <li class="<tags:setActiveMenuItem menuItem="about" selection="${mainSelection}" />"><tags:contentPageUrl urlPath="about">About</tags:contentPageUrl></li>
              <li class="<tags:setActiveMenuItem menuItem="contact" selection="${mainSelection}" />"><tags:contentPageUrl urlPath="contact">Contact</tags:contentPageUrl></li>
              <li class="<tags:setActiveMenuItem menuItem="samples" selection="${mainSelection}" />"><a href="<c:url value="/samples/" />">Samples</a></li>       
        </c:set>
        <c:set var="topNavPullDown">
              <li><a href="<c:url value="/login" />"><i class="glyphicon glyphicon-lock"></i> Log In</a></li>         
        </c:set>     
     </security:authorize>

      <!-- Fixed navbar -->
      <div class="navbar ${topNavStyles} navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${homeUrl}"><spring:message code="appName" /></a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav pull-left">${topNavButtons}</ul>             
            <ul class="nav navbar-nav pull-right">${topNavPullDown}</ul>                        
          </div><!--/.nav-collapse -->
        </div>
      </div>

      <!-- Begin page content -->
      <div class="container">
      	<div>
      	
			<c:if test="${(systemMessageInContext != null) and (systemMessageInContext.enabled) and ((systemMessageInContext.scope.name == 'ALL') or (userPageScope == systemMessageInContext.scope))}">      	
			<div class="panel panel-${systemMessageInContext.type}" id="systemMessagePanel">
			  <div class="panel-heading">
			    <h3 class="panel-title"><button id="systemMessageClose" type="button" class="close">&times;</button> System Message</h3>
			  </div>
			  <div class="panel-body">
			    ${systemMessageInContext.message}
			  </div>
			</div>
			<script type="text/javascript">$('#systemMessageClose').click(function() {$("#systemMessagePanel").remove();});</script>      	
			</c:if>
      	
	      	<c:if test="${flashMessage != null}">
	      		<div class="alert ${flashCss} alert-dismissable">
	      			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	      			${flashMessage}
	      		</div>
	      	</c:if>
	      	<c:if test="${sessionFlashMessage}">
	      	    <c:remove var="sessionFlashMessage" scope="session" />
	      	    <c:remove var="flashMessage" scope="session" />
	      	    <c:remove var="flashCss" scope="session" />
	      	</c:if>
		  	<decorator:body />  
	  	</div>
      </div>
    </div>

    <div id="footer">
      <div class="container">
        <div class="pull-left">
            <p class="text-muted">&copy ${copyrightYear} <spring:message code="appName" /></p>        
        </div>
        <div class="pull-right">
            <p class="text-muted">
            <a href="<c:url value="/index" />">Home</a> / <tags:contentPageUrl urlPath="about">About</tags:contentPageUrl> / <tags:contentPageUrl urlPath="contact">Contact</tags:contentPageUrl> / <tags:contentPageUrl urlPath="help">Help</tags:contentPageUrl> / <tags:contentPageUrl urlPath="terms">Terms</tags:contentPageUrl> / <tags:contentPageUrl urlPath="privacy">Privacy</tags:contentPageUrl>
            </p>        
        </div>        
      </div>
    </div>
    
    <% // The confirmation dialog. %>
	<div class="modal fade" id="confirmDialog" tabindex="-1" role="dialog" aria-labelledby="confirmDialogLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="confirmDialogLabel">Confirm</h4>
	      </div>
	      <div class="modal-body">
	        Are you sure?
	      </div>
	      <div class="modal-footer">
	        <button id="confirmDialogYes" type="button" class="btn btn-primary" data-dismiss="modal">Yes</button>
	        <button id="confirmDialogNo" type="button" class="btn btn-default" data-dismiss="modal">No</button>
	      </div>
	    </div>
	  </div>
	</div> 
	
    <% // The about dialog. %>
	<div class="modal fade" id="aboutDialog" tabindex="-1" role="dialog" aria-labelledby="aboutDialogLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="aboutDialogLabel">About</h4>
	      </div>
	      <div class="modal-body">
			<dl>
			  <dt>Application</dt>
			  <dd><spring:message code="appName" /></dd>				
			  <dt>Build</dt>
			  <dd><spring:message code="buildTimestamp" /></dd>
			  <dt>Profile</dt>
			  <dd>${initialActiveProfiles}</dd>			  
			</dl>
	      </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>	      
	    </div>
	  </div>
	</div> 	
	
    <security:authorize  access="hasRole('ROLE_USER')"> 
    <script type="text/javascript">
    var keepAliveId;
    
    $(function() {
        startTimeout();     
        $('#timeoutModalYes').click(function() {
            $.get('<c:url value="/auth/tickleSession"/>', function(data) {          
            });         
            $('#timeoutModal').modal('hide');
            window.clearTimeout(keepAliveId);
        });     
    });
        
    function startTimeout() {
        window.setInterval(function() {
            $('#timeoutModal').modal('show');           
            keepAliveId = window.setTimeout(function() {                
                window.location.href = "<c:url value="/logout"/>";                                
            },${respondToDialogSeconds});
            
        },${screenInactiveSeconds});      
    }
    </script>

    <div class="modal fade" id="timeoutModal" tabindex="-1" role="dialog"
        aria-labelledby="timeoutModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="timeoutModalLabel">Session About
                        To Expire</h4>
                </div>
                <div class="modal-body">
                    <p>Your session is about to expire. Are you still working?</p>
                </div>
                <div class="modal-footer">
                    <a id="timeoutModalYes" href="#" class="btn btn-primary"> <tags:icon
                            iconName="ok" text="Yes" /></a>
                </div>
            </div>
        </div>
    </div>
    </security:authorize>	
    
  </body>
</html>