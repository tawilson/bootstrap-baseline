<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
  <head>
  	<title>Error</title>
  </head>
  <body>
  	<h1 class="text-danger">System Error <small>Sorry About That</small></h1>
	<p>Our staff has been notified.  Sorry for any inconvenience.</p>
  </body>
</html>