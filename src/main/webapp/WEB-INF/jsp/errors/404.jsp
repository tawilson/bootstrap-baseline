<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
  <head>
  	<title>404 Not Found</title>
  </head>
  <body>
  
  	<h1 class="text-danger">404<small>Resource Not Found</small></h1>
	<p>We are unable to locate what you are looking for.</p>
  </body>
</html>