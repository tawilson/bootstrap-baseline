<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
  <head>
    <title>403 Access Denied</title>
  </head>
  <body>
  
    <h1 class="text-danger">403<small>Access Denied</small></h1>
    <p>You do not have permission to the requested action or resource.</p>
  </body>
</html>