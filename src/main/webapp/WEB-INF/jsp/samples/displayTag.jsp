<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Display Tag</title>
<content tag="leftMenuSelection">displayTag</content>
</head>
<body>
	<h4>Display Tag</h4>

	<c:set var="pageUri"><c:url value="/samples/displayTag" /></c:set>
		<display:table htmlId="messageTable" id="message" name="messageList" class="table table-bordered table-hover table-condensed small display-tag-table" requestURI="${pageUri}">
    		<display:column title="Message">
                <a href="<c:url value="/samples/message" />/${message.id}">${message.message}</a>            		
    		</display:column>
    		<display:column property="beenRead" title="Read" />
    		<display:column title="Timestamp">
    			<tags:formatDate date="${message.created}" />
    		</display:column>
    		<display:column>
    			<a href="<c:url value="/samples/message" />/${message.id}" class="btn btn-default btn-xs">edit</a>
    		</display:column>    
		</display:table>
		
		<script type="text/javascript">
		$(function() {
			initDisplayTable('messageTable', null, true);		
		});
		</script>
	
</body>
</html>