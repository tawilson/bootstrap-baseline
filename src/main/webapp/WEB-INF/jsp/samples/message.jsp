<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>HTML5 Forms</title>
	
	<script src="<c:url value="/resources/js/moment.min.js" />"></script>	
    <link href="<c:url value="/resources/css/bootstrap-datetimepicker.min.css" />" rel="stylesheet">	
	<script src="<c:url value="/resources/js/bootstrap-datetimepicker.min.js" />"></script>
		
	<content tag="leftMenuSelection">html5form</content>
</head>
<body>

	<h4>HTML5 Forms</h4>
	    	
        <tags:objectLevelErrors objectName="message" />  
	
		<form class="form-horizontal" role="form" method="POST" action="<c:url value="/samples/message/save" />">
			<input type="hidden" name="id" value="${message.id}" />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		  <div class="form-group">
		    <label for="message" class="col-sm-2 control-label">Message</label>
		    <div class="col-sm-5">
		      <input type="text" maxlength="50" required="required" class="form-control" id="message" name="message" value="${message.message}">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="created" class="col-sm-2 control-label">Created</label>
		    <div class="col-sm-5">
				<input type="datetime" value="<fmt:formatDate value="${message.created}" pattern="h:mm a, M/d/yyyy" />" class="form-control" data-date-format="h:mm A, M/D/YYYY" id="created" name="created" required="required">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-5">
		      <div class="checkbox">
		        <label>
		          <input type="hidden" value="on" name="_beenRead">
		          <input type="checkbox" id="beenRead" name="beenRead" <c:if test="${message.beenRead}">checked="checked"</c:if>> Read
		        </label>
		      </div>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-5">
		    	<div class="pull-left">
		      		<button type="submit" class="btn btn-primary">Save</button>
		      		<c:if test="${not isNew}">
		      			<button id="deleteMessage" type="button" class="btn btn-default">Delete</button>
		      			<script type="text/javascript">
		      				$('#deleteMessage').click(function() {
		      					showConfirmRedirectOnYes('<c:url value="/samples/message/" />' + ${message.id} + '/delete')
		      				});
		      			</script>
		      		</c:if>
		      	</div>
		      	<div class="pull-right">
		      		<button type="reset" class="btn btn-default pull-right">Cancel</button>
		      	</div>		      
		    </div>
		  </div>
		</form>
		
		<script type="text/javascript">
		  $(function() {
		    $('#created').datetimepicker({
		    	pickDate: true,
		    	pickTime: true,
		      	useSeconds: false
		    });
		  });
		</script>			
	
</body>
</html>