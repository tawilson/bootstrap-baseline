<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Token Input</title>
<content tag="leftMenuSelection">tokeninput</content>

<link href="<c:url value="/resources/css/token-input.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.tokeninput.js" />"></script>
</head>
<body>
	<h4>Token Input</h4>
	
	<div class="panel panel-default">
	  <div class="panel-body">
	  
		<form class="form-inline" role="form" class="small-vert-margin">
			<div class="form-group">
				<input type="hidden" name="selectedMessage" id="selectedMessage" />
				<tags:tokenInput tokenLimit="1" input="#selectedMessage" sourceUrl="/samples/tokeninput/search" preventDuplicates="true" minChars="3" propertyToSearch="message" />
			</div>
			<button id="messageOneBtn" type="button" class="btn btn-default">Edit</button>	
			<script type="text/javascript">
				$('#messageOneBtn').click(function() {
					var messageId = $('#selectedMessage').val();
					if(messageId == '') {
						alert("Please Select A Message");
					} else {
						window.location = '<c:url value="/samples/message/" />' + messageId;
					}
				});
			</script>	
		</form>	  
	  
	  </div>
	</div>	
	
	<div class="panel panel-default">
	  <div class="panel-body">
	  
		<form role="form">
			<input type="hidden" name="selectedMessages2" id="selectedMessages2" />
			<tags:tokenInput input="#selectedMessages2" sourceUrl="/samples/tokeninput/search" preventDuplicates="true" minChars="3" propertyToSearch="message" />		
		</form>		  
	  
	  </div>
	</div>	
	
</body>
</html>