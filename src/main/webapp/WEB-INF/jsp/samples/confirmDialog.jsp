<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Confirm Dialog</title>
<content tag="leftMenuSelection">confirmDialog</content>
</head>
<body>
	<h4>Confirm Dialog</h4>
		
	<button id="showConfirmDialog"  class="btn btn-primary">
  		Show Confirm Dialog
	</button>	
	
	<button id="showConfirmRedirectDialog"  class="btn btn-primary">
  		Show Confirm Redirect Dialog
	</button>	
		
	<script type="text/javascript">
		$('#showConfirmDialog').click(function() {
			showConfirm(function() {
				alert("Yes Clicked");
			});  // the no click callback is optional
		});
		
		$('#showConfirmRedirectDialog').click(function() {
			showConfirmRedirectOnYes('<c:url value="/samples/confirmDialog/redirect" />');
		});		
		
	</script>	
	
</body>
</html>