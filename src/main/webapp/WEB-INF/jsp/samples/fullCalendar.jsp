<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
	<title>Full Calendar</title>
	<link href="<c:url value="/resources/css/fullcalendar.css" />" rel="stylesheet">
	<script src="<c:url value="/resources/js/fullcalendar.min.js" />"></script>
	<content tag="leftMenuSelection">fullCalendar</content>
</head>
<body>
		
	<h4>Full Calendar <img id="entries-wait" alt="wait" src="<c:url value="/resources/img/wait.gif" />"></h4>
	
	<div id="calendar" class="large-vert-margin"></div>

	<script type="text/javascript">
	$(function () {
	    $('#calendar').fullCalendar({
		    eventSources: [
		        {
		            url: '<c:url value="/samples/fullCalendar/fetch"/>',
		            type: 'GET',
		        }
		    ],
		    loading : function(isLoading, view) {
		    	if(isLoading) {
		    		$('#entries-wait').show();
		    	} else {
		    		$('#entries-wait').hide();
		    	}
		    },
    	});
    	
    	$('#entries-wait').hide();
	});
	</script>
	
</body>
</html>