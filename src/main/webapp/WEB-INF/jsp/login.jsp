<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
</head>
<body>

    <div class="row large-vert-margin">   
    
        <div class="col-md-6 col-md-offset-3">
	
			<tags:showAlert type="alert-danger" id="loginErrorAlert" classes="hide" dismissable="true">
				<spring:message code="login.error" />
			</tags:showAlert>	
		
			<div class="panel panel-default">
			
				<div class="panel-heading clearfix">
				    <div class="pull-left"><h3 class="panel-title">Please Sign In</h3></div>
				    <div class="pull-right">or <a href="<c:url value="/register" />">Sign Up</a></div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal"  action="<c:url value="/login" />" method="post" role="form">
					    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
						<div class="form-group">
						    <label class="col-sm-3 control-label" for="username">Email Address</label>
						    <div class="col-sm-6">
							<input type="email" class="form-control" name="username" autofocus="autofocus" required="required">
							</div>
						</div>
						<div class="form-group">
						    <label class="col-sm-3 control-label" for="password">Password</label>
						    <div class="col-sm-6">
							<input type="password" class="form-control" name="password" autocomplete="off" required="required">
							</div>
						</div>			
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
								<div class="checkbox">
								  <label>
								    <input type="checkbox" name="remember-me" id="remember-me">
								    Remember Me
								  </label>
								</div>                            
                            </div>
                        </div>							
						<div class="form-group">
						    <div class="col-sm-offset-3 col-sm-6">			
							    <button type="submit" class="btn btn-primary btn-block">Log In</button>
							</div>
						</div>
						<div class="form-group text-right">
                            <div class="col-sm-offset-3 col-sm-6">  						
							    <a href="<c:url value="/lostpassword" />" class="text-muted">Forgot your password?</a>
							</div>
						</div>				
					</form>
				</div>
	        </div>
        </div>
    </div>
	
	<script type="text/javascript">
	$(function() {
		if(${param.login_error eq 'true'}) {
			$('#loginErrorAlert').removeClass('hide');
		}
	});
	</script>

</body>
</html>