<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Application Properties</title>
<content tag="leftMenuSelection">appProps</content>
</head>
<body>
    
<tags:pageTitle title="Application Properties" />

	<form class="form-horizontal" role="form" class="small-vert-margin" action="<c:url value="/auth/admin/appProps/save" />">
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
      <div class="form-group">
        <div class="col-sm-5">	  
            <select class="form-control" id="appPropId" name="appPropId" required="required">
                <option selected="selected" value="">-- select --</option>
                <c:forEach items="${appPropList}" var="appProp">                
                    <option value="${appProp.id}">${appProp.key}</option>
                </c:forEach>                
            </select>
        </div>
      </div>	 

      <div class="form-group">
        <div class="col-sm-5">
           <input type="text" class="form-control" id="appPropValue" name="appPropValue" required="required">
           <span id="helpBlock" class="help-block"><strong><i class="glyphicon glyphicon glyphicon-exclamation-sign"></i>Be sure to enter a value for the correct data type: </strong><br />
           <span id="appPropType"></span></span>           
        </div>
      </div> 
	  
      <div class="form-group">
        <div class="col-sm-5">	  
	       <button type="submit" class="btn btn-default">Save</button>
        </div>
      </div>	  
	</form>
	
	<script type="text/javascript">
	
    $(function() {
    	$( "#appPropId" ).change(function() {
    		var appPropId = $(this).val();
    		if(!isBlank(appPropId)) {
    		   $.get("<c:url value="/auth/admin/appProps/" />" + appPropId, function(data) {
    			   $("#appPropValue").val(data.data.applicationProperty.value)
    			   $("#appPropType").html(data.data.applicationProperty.type)
    		   });    		   
            } else {
            	$("#appPropValue").val("");
                $("#appPropType").html("");            	
            }
    	});                    
    });	
	
	</script>

</body>
</html>