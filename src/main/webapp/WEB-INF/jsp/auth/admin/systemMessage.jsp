<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>System Message</title>
		
	<content tag="leftMenuSelection">systemMessage</content>
</head>
<body>
    <tags:pageTitle title="System Message" />
	
    <form class="form-horizontal" role="form" method="POST" action="<c:url value="/auth/admin/systemMessage/save" />">
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
     
      <tags:formControl labelWidth="col-sm-2" propertyName="message" label="Message" optionWidth="col-sm-5" path="systemMessage.message">   
          <textarea class="form-control" rows="5" required="required" id="message" name="message">${systemMessage.message}</textarea>
      </tags:formControl>      
      
      <tags:formControl labelWidth="col-sm-2" propertyName="scope" label="Scope" optionWidth="col-sm-5" path="systemMessage.scope">
        <select id="scope" name="scope" class="form-control">
            <c:forEach items="${messageScopes}" var="scope">                                     
                <option value="${scope}" <c:if test="${systemMessage.scope eq scope}">selected</c:if>>${scope}</option>
            </c:forEach>
        </select>
      </tags:formControl>
      
      <tags:formControl labelWidth="col-sm-2" propertyName="type" label="Type" optionWidth="col-sm-5" path="systemMessage.type">
        <select id="type" name="type" class="form-control">
            <c:forEach items="${messageTypes}" var="type">                                     
                <option value="${type}" <c:if test="${systemMessage.type eq type}">selected</c:if>>${type}</option>
            </c:forEach>
        </select>
      </tags:formControl>           

      <tags:formRadio propertyId="enabled" propertyName="enabled" labelWidth="col-sm-2" optionWidth="col-sm-5" label="Enabled" path="systemMessage.enabled" />
      
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-5">
            <div class="pull-left">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="pull-right">
                <button type="reset" class="btn btn-default pull-right">Cancel</button>
            </div>            
        </div>
      </div>
    </form>	
	    		
</body>
</html>