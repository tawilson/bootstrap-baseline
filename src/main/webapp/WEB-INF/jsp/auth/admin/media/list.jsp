<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Media</title>

<script src="<c:url value="/resources/js/SimpleAjaxUploader.min.js" />"></script>
<script src="<c:url value="/resources/js/mustache.min.js" />"></script>

<content tag="leftMenuSelection">media</content>

</head>
<body>
		<tags:pageTitle title="Media" />
	
		<div class="well well-sm">
			<div >
				<button id="saveMediaButton" class="btn btn-default btn-sm">Upload</button>
				<div id="resourceProgressBar"
					class="progress progress-striped">
					<div id="progressBar" class="progress-bar" role="progressbar"
						aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
						style="width: 0%;"></div>
				</div>
			</div>
		</div>
        
        <div id="mediaList" class="row" style="display:flex; flex-wrap: wrap;">

        </div>
        
        <script type="text/x-mustache" id="mediaTemplate">
             <div id="mediaContainer{{id}}" class="col-sm-5 col-md-3">
                <div class="thumbnail">
                  <img class="img-thumbnail" src="<c:url value="/media/{{originalFilename}}" />">
                  <div class="caption">
                    <dl>
                        <dt>File Name</dt><dd title="{{originalFilename}}">{{formattedOriginalFilename}}</dd>
                        <dt>Size</dt><dd>{{formattedSize}} kb</dd>
                        <dt>Uploaded</dt><dd>{{formattedCreateDate}}</dd>
                        <dt>Content Type</dt><dd>{{contentType}}</dd>
                    </dl>                   
                    <p><a href="<c:url value="/media/{{originalFilename}}" />" target="_blank" class="btn btn-default btn-xs" role="button">View</a> 
                    <a href="#" class="btn btn-default btn-xs" role="button" onclick="deleteMedia({{id}})">Delete</a></p>
                  </div>
                </div>
              </div>          
        </script>        
               
		<script type="text/javascript">
		
		   var mediaTemplate = $('#mediaTemplate').html();
		   Mustache.parse(mediaTemplate);   // optional, speeds up future uses		
		
		   function deleteMedia(mediaId) {
			   		   
			   showConfirm(function() {			
	               $.get("<c:url value="/auth/admin/media/delete/" />" + mediaId, function(data) {                          
	                   $("#mediaContainer" + mediaId).fadeOut(300, function(){                 
	                       $(this).remove();
	                   }); 
	               });			   		   
			   });
		   }
		
			var progressBar = $('#progressBar');
			var uploader = new ss.SimpleUpload({
				button : 'saveMediaButton', // HTML element used as upload button
				url : '<c:url value="/auth/admin/media/save" />?${_csrf.parameterName}=${_csrf.token}', // URL of server-side upload handler
				name : 'file', // Parameter name of the uploaded file
				responseType : 'json',
				allowedExtensions : [ 'jpg', 'jpeg', 'png', 'gif' ],
				maxSize : 20480, // kilobytes			
				multipart : true,
				onSubmit : function(filename, extension) {
					$('#resourceProgressBar').show();
					this.setProgressBar(progressBar);
				},
				onComplete : function(filename, response) {
					$('#resourceProgressBar').hide();
					if(response.success) {
	                    var renderedMedia = Mustache.render(mediaTemplate, response.data.media);
	                    $('#mediaList').prepend(renderedMedia).hide().fadeIn('slow');   						
					} else {
						alert(response.message);
					}
				}
			});
			
			function showMedia(media) {
   				
			}
	
			$(function() {
				$('#resourceProgressBar').hide();
				initDisplayTable('mediaTable', null, false);   
				
	            $.get("<c:url value="/auth/admin/media/list" />", function(data) {
	            	var mediaList = data.data.mediaList;                    
                    for (var i in mediaList) {   
                        var renderedMedia = Mustache.render(mediaTemplate, mediaList[i]);
                        $('#mediaList').append(renderedMedia);                    	                
                    }
	            });  			
			});
		</script>
	
</body>
</html>