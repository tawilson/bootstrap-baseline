<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Audit Records</title>
<content tag="leftMenuSelection">audit</content>
</head>
<body>
<tags:pageTitle title="Audit Records" />

	<c:set var="pageUri"><c:url value="/auth/admin/audit" /></c:set>
		<display:table htmlId="auditRecordTable" id="auditRecord" name="auditRecordList" class="table table-bordered table-hover table-condensed small display-tag-table" requestURI="${pageUri}">
    		<display:column property="user.username" title="Username" />
    		<display:column property="resourceType" title="Resource Type" />
            <display:column title="Resource Id">
                <c:if test="${empty auditRecord.resourceId}">null</c:if>
                <c:if test="${!empty auditRecord.resourceId}">${auditRecord.resourceId}</c:if>
            </display:column>
            <display:column property="action" title="Action" />    		
    		<display:column title="Timestamp">
    			<tags:formatDate date="${auditRecord.timestamp}" />
    		</display:column>   
		</display:table>
		
		<script type="text/javascript">
		$(function() {
			initDisplayTable('auditRecordTable');		
		});
		</script>
	
</body>
</html>