<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Users</title>
<content tag="leftMenuSelection">users</content>
</head>
<body>
	<tags:pageTitle title="Users" />
	
        <div class="well well-sm">
            <a href="<c:url value="/auth/admin/users/create" />" class="btn btn-default btn-sm">New User</a>
        </div>	
	
		<c:set var="pageUri"><c:url value="/auth/admin/users" /></c:set>
			<display:table htmlId="userTable" id="user" name="userList" class="table table-bordered table-hover table-condensed small display-tag-table" requestURI="${pageUri}">
	            <display:column title="Username">
	                <a href="<c:url value="/auth/admin/users" />/${user.id}">${user.username}</a>             
	            </display:column>		
	    		<display:column property="accountNonExpired" title="Account NonExpired" />
	    		<display:column property="accountNonLocked" title="Account NonLocked" />
	    		<display:column property="credentialsNonExpired" title="Credentials NonExpired" />
	    		<display:column property="enabled" title="Enabled" />
	    		<display:column title="Roles">
	    		  <c:forEach items="${user.authorities}" var="authority">${authority.authority}&nbsp;</c:forEach>    		  
	    		</display:column>
			</display:table>
			
			<script type="text/javascript">
			$(function() {
				initDisplayTable('userTable', null, true);		
			});
			</script>
			
	<tags:pageTitle title="Deleted Users" />
	
	    <c:if test="${empty deletedUsers}">
	        No deleted Users
	    </c:if>
	    
	    <c:if test="${not empty deletedUsers}">
	    <form class="form-inline" role="form">
	        <select id="selectedUser" name="user" class="form-control">
	            <option value="">-- Select --</option>        
	            <c:forEach items="${deletedUsers}" var="user">
	                <option value="${user.id}">${user.username}</option>            
	            </c:forEach>            
	        </select>
	        <button id="unDeleteButton" type="button" class="btn btn-primary">Un-delete</button>
	    </form>
	    </c:if>    
	        
	        <script type="text/javascript">
	        $(function() {
	            $('#unDeleteButton').click(function() {
	                var selectedUserId = $('#selectedUser').val();              
	                if(selectedUserId != "") {
	                    showConfirmRedirectOnYes('<c:url value="/auth/admin/users/" />' + selectedUserId + '/undelete')
	                }
	            });
	        });
	        </script>		
	
</body>
</html>