<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Edit User</title>
<content tag="leftMenuSelection">users</content>
</head>
<body>

<tags:pageTitle title="Edit User: ${user.username}" />

<div class="large-vert-margin">
	<form class="form-horizontal" role="form" method="POST" action="<c:url value="/auth/admin/users/save" />">
	    <input type="hidden" name="id" value="${user.id}" />
	    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	     	    
	   <tags:formGroupTextInput label="User Name" propertyName="username" maxlength="100" path="user.username" propertyId="username" required="true" labelWidth="col-sm-2" inputWidth="col-sm-5" readonly="${newUser}" />
	   <tags:formRadio propertyId="enabled" propertyName="enabled" labelWidth="col-sm-2" optionWidth="col-sm-5" label="Enabled" path="user.enabled" />   
	   
	   <tags:formControl labelWidth="col-sm-2" propertyName="authorities" label="Roles" optionWidth="col-sm-5" path="user.authorities">
	       <c:forEach items="${authorities}" var="authority">
			<div class="checkbox">
			  <label>
			    <input name="authority" type="checkbox" value="${authority.authority}" <c:if test="${cfn:contains(user.authorities,authority)}">checked="checked"</c:if>>
			    ${authority.authority}
			  </label>
			</div>
		  </c:forEach>
	   </tags:formControl>
	    
	    <div class="form-group">
	      <div class="col-sm-offset-2 col-sm-5">
	          <div class="pull-left">
	              <button type="submit" class="btn btn-primary">Save</button>
	              <c:if test="${(not user.deleted) and canDelete}">
                  <button id="deleteUser" type="button" class="btn btn-default">Delete</button>
                  <script type="text/javascript">
                  $('#deleteUser').click(function() {
                	  showConfirmRedirectOnYes('<c:url value="/auth/admin/users/" />' + ${user.id} + '/delete')
                  });
                  </script>	        
                  </c:if>      
	          </div>
	          <div class="pull-right">
	              <tags:reloadButton label="Cancel" btnClass="btn-default" />
	          </div>            
	      </div>
	    </div>                                 
	</form>
</div>
    
</body>
</html>