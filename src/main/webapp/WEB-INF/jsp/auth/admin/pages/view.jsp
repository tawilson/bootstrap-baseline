<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>Content Page</title>
		
	<content tag="leftMenuSelection">pages</content>
</head>
<body> 				
		<tags:form formTitle="Content Page" url="/auth/admin/pages/save" modelObj="${contentPage}" modelObjName="contentPage" >
		
            <tags:formGroupTextInput label="Browser Title" propertyName="browserTitle" maxlength="50" path="contentPage.browserTitle" propertyId="browserTitle" required="true" labelWidth="col-sm-2" inputWidth="col-sm-5" />
            <tags:formGroupTextInput label="Page Title" propertyName="pageTitle" maxlength="50" path="contentPage.pageTitle" propertyId="pageTitle" required="true" labelWidth="col-sm-2" inputWidth="col-sm-5" />
            <tags:formGroupTextInput label="URL Path" propertyName="urlPath" maxlength="50" path="contentPage.urlPath" propertyId="urlPath" required="true" labelWidth="col-sm-2" inputWidth="col-sm-5" />                     
            
	       <tags:formControl labelWidth="col-sm-2" propertyName="content" label="Content" optionWidth="col-sm-10" path="contentPage.content">
                <textarea class="form-control" rows="10" cols="30" name="content" id="content">${contentPage.content}</textarea>
                <div class="small-vert-margin clearfix">
                    <button id="showPreviewButton" class="btn btn-default btn-sm pull-right" type="button">show preview</button>
                </div>
                
                <div id="contentPreview" class="mid-vert-margin">                
					<div class="panel panel-default">
					  <div class="panel-body" id="contentPreviewBody">
					    Panel content
					  </div>
					  <div class="panel-footer">
					  Click anywhere outside of the editing window to update the content preview.
					  </div>
					</div>                                
                </div>
	       </tags:formControl>    
	       
	       <script>
	       
	       $(function() {
	    	  $('#contentPreview').hide();
	    	  $('#showPreviewButton').click(function() {
	    		  $('#contentPreview').toggle();
	    	  });
	    	  
	    	  function updatePreviewPanel() {
	    		  $("#contentPreviewBody").empty().append($('#content').val());
	    	  }
	    	  
	    	  $('#content').blur(function() {
	    		  updatePreviewPanel();
	    	  });
	    	  updatePreviewPanel();
	       });
	       
	       </script>
	       
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="pull-left">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <c:if test="${not isNew}">
                        <button id="deleteContentPage" type="button" class="btn btn-default">Delete</button>
                        <script type="text/javascript">
                            $('#deleteContentPage').click(function() {
                                showConfirmRedirectOnYes('<c:url value="/auth/admin/pages/delete/" />' + ${contentPage.id})
                            });
                        </script>
                    </c:if>
                </div>
                <div class="pull-right">
                    <button type="reset" class="btn btn-default pull-right">Cancel</button>
                </div>            
            </div>
          </div>	                  		
		
		</tags:form>
</body>
</html>