<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Content Pages</title>
<content tag="leftMenuSelection">pages</content>
</head>
<body>
<tags:pageTitle title="Pages" />

    <div class="well well-sm">
        <a href="<c:url value="/auth/admin/pages/new" />" class="btn btn-default btn-sm">New Page</a>
    </div>
    
        <c:set var="pageUri"><c:url value="/auth/admin/pages" /></c:set>
        <display:table htmlId="pageTable" id="contentPage" name="pageList" class="table table-bordered table-hover table-condensed small display-tag-table" requestURI="${pageUri}">
            <display:column title="Page Title">
                <a href="<c:url value="/auth/admin/pages" />/${contentPage.id}">${contentPage.pageTitle}</a>             
            </display:column>       
            <display:column property="browserTitle" title="Browser Title" />
            <display:column property="urlPath" title="URL Path" />
        </display:table>
        
        <script type="text/javascript">
        $(function() {
            initDisplayTable('pageTable', null, true);      
        });
        </script>    
	
</body>
</html>