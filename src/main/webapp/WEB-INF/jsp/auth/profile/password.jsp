<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html>
<head>
<title>Change Password</title>
<content tag="leftMenuSelection">password</content>
</head>
<body>

<tags:pageTitle title="Change Your Password" />

<form class="form-horizontal" role="form" method="POST" action="<c:url value="/auth/profile/password" />">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    <tags:formGroupTextInput type="password" label="Current Password" propertyName="currentPassword" maxlength="12" path="resetPassword.currentPassword" propertyId="currentPassword" required="true" labelWidth="col-sm-3" inputWidth="col-sm-3" autocomplete="off"/>
    
    <tags:formGroupTextInput type="password" label="New Password" propertyName="password" maxlength="12" path="resetPassword.password" propertyId="password" required="true" labelWidth="col-sm-3" inputWidth="col-sm-3" autocomplete="off"/>
    <tags:formGroupTextInput type="password" label="Confirm New Password" propertyName="password2" maxlength="12" path="resetPassword.password2" propertyId="password2" required="true" labelWidth="col-sm-3" inputWidth="col-sm-3" autocomplete="off"/>   
    
    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-3">
              <button type="submit" class="btn btn-primary">Change Password</button>
      </div>
    </div>      

</form>

    
</body>
</html>