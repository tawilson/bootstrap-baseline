<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${contentPage.browserTitle}</title>
<content tag="mainSelection">${contentPage.urlPath}</content>
</head>
<body>
	<tags:pageTitle title="${contentPage.pageTitle}" />
	
	${contentPage.content}
	
</body>
</html>