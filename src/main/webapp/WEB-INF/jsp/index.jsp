<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title> <spring:message code="appSlogan" /> </title>
</head>
<body>		

    <c:out value="${content}" escapeXml="false" />
 
</body>
</html>