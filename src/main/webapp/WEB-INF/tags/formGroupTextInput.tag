<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"
%><%@ attribute name="type" required="false"
%><%@ attribute name="label" required="true"
%><%@ attribute name="path" required="true"
%><%@ attribute name="propertyName" required="true"
%><%@ attribute name="propertyId" required="true"
%><%@ attribute name="helpBlock" required="false"
%><%@ attribute name="maxlength" required="true"
%><%@ attribute name="required" required="false" type="java.lang.Boolean"
%><%@ attribute name="labelWidth" required="true"
%><%@ attribute name="inputWidth" required="true"
%><%@ attribute name="readonly" required="false" type="java.lang.Boolean"
%><%@ attribute name="autocomplete" required="false"
%><spring:bind path="${path}">
<c:set var="inputType">${type}</c:set><c:if test="${empty type}"><c:set var="inputType">text</c:set></c:if>       
<div class="form-group <c:if test="${status.error}">has-error</c:if>">
  <label for="${propertyName}" class="${labelWidth} control-label">${label}</label>
  <div class="${inputWidth}">
    <input type="${inputType}" maxlength="${maxlength}" <c:if test="${required}">required="required"</c:if> <c:if test="${readonly}">disabled</c:if> class="form-control" id="${propertyId}" name="${propertyName}" value="${status.value}" autocomplete="${autocomplete}">
    <span class="help-block">
    <c:if test="${status.error}">
    <div class="small-vert-margin"><i class="glyphicon glyphicon-flag"></i> <c:forEach items="${status.errorMessages}" var="error">${error}<br/></c:forEach></div>
    </c:if>
    ${helpBlock}
    </span>        
  </div>
</div>   
</spring:bind>