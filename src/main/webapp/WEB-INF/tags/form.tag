<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"
%><%@ attribute name="formTitle" required="true"
%><%@ attribute name="url" required="true"
%><%@ attribute name="modelObj" required="true" type="java.lang.Object"
%><%@ attribute name="modelObjName" required="true"
%><tags:pageTitle title="${formTitle}" />   
<tags:objectLevelErrors objectName="${modelObjName}" />
<form class="form-horizontal" role="form" method="POST" action="<c:url value="${url}" />">
            <input type="hidden" name="id" value="${modelObj.id}" />
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <jsp:doBody/>
</form>