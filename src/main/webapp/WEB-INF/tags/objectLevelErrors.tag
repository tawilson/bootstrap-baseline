<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"%><%@ attribute name="objectName" required="true"%>
<spring:bind path="${objectName}">
    <c:if test="${status.error}">
        <div class="alert alert-danger" role="alert">
            <c:forEach items="${status.errorMessages}" var="error">
                <p>${error}</p>
            </c:forEach>
        </div>
    </c:if>
</spring:bind>
