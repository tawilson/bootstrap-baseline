<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"
%><%@ attribute name="type" required="true" 
%><%@ attribute name="dismissable" required="false" type="Boolean"
%><%@ attribute name="id" required="false"
%><%@ attribute name="classes" required="false"
%>
<div id="${id}" class="alert ${type} ${classes} <c:if test="${dismissable}">alert-dismissable</c:if>">
	<c:if test="${dismissable}"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></c:if>
<jsp:doBody/>
</div>