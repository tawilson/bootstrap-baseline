<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"
%><%@ attribute name="content" required="true"
%><%@ attribute name="maxLength" required="true" type="Integer"
%><span title="${content}"><c:choose><c:when test="${fn:length(content) > (maxLength-3)}">${fn:substring(content,0,maxLength-3)}...</c:when><c:otherwise>${content}</c:otherwise></c:choose></span>