<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"
%><%@ attribute name="input" required="true"
%><%@ attribute name="sourceUrl" required="true"
%><%@ attribute name="minChars" required="true" 
%><%@ attribute name="propertyToSearch" required="true" 
%><%@ attribute name="preventDuplicates" required="true" type="java.lang.Boolean"
%><%@ attribute name="resultsLimit" required="false"
%><%@ attribute name="tokenLimit" required="false"
%><c:if test="${empty resultsLimit}"><c:set var="resultsLimit">null</c:set></c:if>
<c:if test="${empty tokenLimit}"><c:set var="tokenLimit">null</c:set></c:if>
<script type="text/javascript">
$(function() {
	$("${input}").tokenInput("<c:url value="${sourceUrl}" />",{minChars:${minChars},propertyToSearch:'${propertyToSearch}',preventDuplicates:${preventDuplicates},resultsLimit:${resultsLimit},tokenLimit:${tokenLimit}});
});
</script>