<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"
%><%@
attribute name="iconName" required="true"%><%@
attribute name="text" required="false"%><%@
attribute name="size" required="false"%>
<c:if test="${size != null}"><c:set var="iconSize" value="fa-${size}x" /></c:if>
<i class="fa fa-${iconName} ${iconSize}"></i> ${text}
