<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp"
%><%@ attribute name="label" required="true"
%><%@ attribute name="path" required="true"
%><%@ attribute name="propertyName" required="true"
%><%@ attribute name="helpBlock" required="false"
%><%@ attribute name="required" required="false" type="java.lang.Boolean"
%><%@ attribute name="labelWidth" required="true"
%><%@ attribute name="optionWidth" required="true"
%><%@ attribute name="readonly" required="false" type="java.lang.Boolean"
%><spring:bind path="${path}">
<div class="form-group <c:if test="${status.error}">has-error</c:if>">
  <label for="${propertyName}" class="${labelWidth} control-label">${label}</label>
  <div class="${optionWidth}">
  
        <jsp:doBody/>
    
    <span class="help-block">
    <c:if test="${status.error}">
    <div class="small-vert-margin"><i class="glyphicon glyphicon-flag"></i> <c:forEach items="${status.errorMessages}" var="error">${error}<br/></c:forEach></div>
    </c:if>
    ${helpBlock}
    </span>        
  </div>
</div>   
</spring:bind>