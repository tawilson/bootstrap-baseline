function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function showConfirm(yesCallback,noCallback) {
	$('#confirmDialog').modal('show');
	$('#confirmDialogYes').off('click').click(function() {
		if($.isFunction(yesCallback)) {
			yesCallback();			
		}		
	});
	$('#confirmDialogNo').off('click').click(function() {
		if($.isFunction(noCallback)) {
			noCallback();			
		}		
	});	
}

function showConfirmRedirectOnYes(url) {
	showConfirm(function() {
		window.location = url;
	});
}

/* Enhances and styles the default display table.
 * rowHandlerCallback is a function that will have the opportunity to perform additional processing on each row
 * the first cell should have a link and it will be used to make the row clickable
 */
function initDisplayTable(tableId, rowHandlerCallback, linkable) {
    
    // Tables with the display-table will be styled.
    $('#' + tableId + ' tr').each(function(index,row) {
        if(index != 0) {
            
            // this param is not required so check 
            if (rowHandlerCallback != undefined) {
                rowHandlerCallback(index,row);
            }
            
            if(linkable) {
            
	            // Add a pointer to the row
	            $(row).addClass('hand');
	            
	            // the first cell must have an href.  This will be used to make the row clickable.
	            var link = $(this).find('td > a');
	            $(link).addClass('unstyle-link');
	            
	            // Make all but the last column clickable
	            for (i=0; i < row.cells.length-1 ; i++) {
	                $(row.cells[i]).click(function() {
	                    window.location = link.attr('href');
	                });                    
	            }
            }
            
        }
    });    

}
/* END initDisplayTable */