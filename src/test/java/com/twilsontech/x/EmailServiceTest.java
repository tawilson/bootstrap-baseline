package com.twilsontech.x;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import com.twilsontech.x.service.EmailService;
import com.twilsontech.x.service.EmailService.EmailServiceBuilder;

public class EmailServiceTest {
	
	private int port = 7474;
	private SimpleSmtpServer smtpServer;
	private String override = "override@bootstrapbaseline.com";
	
	public EmailServiceTest() {
	}
	
	@Before
	public void beforeClass() {
		smtpServer = SimpleSmtpServer.start(port);
	}
	
	@After
	public void afterClass() {
		smtpServer.stop();
		smtpServer = null;
	}
	
	private EmailServiceBuilder newBuilder() {
		EmailServiceBuilder builder = new EmailServiceBuilder("localhost");
		builder.port(port);
		builder.from("\"Test\" <test@bootstrapbaseline.com>");
		builder.replyTo("\"Please DO NOT REPLY\" <noreply@bootstrapbaseline.com>");
		return builder;
	}

	@Test
	public void testSendAdminEmail() {
		EmailService emailService = newBuilder().adminRecipient("admin@bootstrapbaseline.com").build();
		Map<String, Object> adminEmailInfo = new HashMap<>();
		adminEmailInfo.put("Key1", "value1");
		adminEmailInfo.put("Key2", "value2");
		String subject = "testSendAdminEmail";
		emailService.sendAdminEmail(subject, adminEmailInfo);
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertEquals(subject, message.getHeaderValue("Subject"));
		String body = message.getBody();
		assertTrue(body.contains("Key1"));
		assertTrue(body.contains("value1"));
		assertTrue(body.contains("Key2"));
		assertTrue(body.contains("value2"));
		assertEquals("admin@bootstrapbaseline.com", message.getHeaderValue("To"));		 		
	}
	
	@Test
	public void testSendMessage() {
		EmailService emailService = newBuilder().build();
		String subject = "testSendAdminEmail";
		String mailMessage = "testMessage";
		emailService.sendMessage("user@bootstrapbaseline.com", mailMessage, subject);
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertEquals(subject, message.getHeaderValue("Subject"));
		assertEquals(mailMessage, message.getBody());
		assertEquals("user@bootstrapbaseline.com", message.getHeaderValue("To"));		 
		
	}	
	
	@Test
	public void testSendMessageMultipleTo() {
		EmailService emailService = newBuilder().build();
		String subject = "testSendAdminEmail";
		String mailMessage = "testMessage";
		emailService.sendMessage(new String[]{"user@bootstrapbaseline.com","user2@bootstrapbaseline.com"}, mailMessage, subject);
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertEquals(subject, message.getHeaderValue("Subject"));
		assertEquals(mailMessage, message.getBody());
		assertTrue(message.getHeaderValue("To").contains("user@bootstrapbaseline.com"));
		assertTrue(message.getHeaderValue("To").contains("user2@bootstrapbaseline.com"));
	}	
	
	@Test
	public void testSendTemplatedMessageDefaultFrom() {

		Map<String, Object> defaultModel = new HashMap<>();
		defaultModel.put("defaultMessage", "Default Message");
		EmailService emailService = newBuilder().addTemplateModel(defaultModel).build();
		
		String subject = "testSendAdminEmail";
		Map<String, Object> messageModel = new HashMap<>();
		messageModel.put("testMessage", "Test Message");		
		emailService.sendTemplatedMessage("user@bootstrapbaseline.com", subject, messageModel, "template/test.html");
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertTrue(message.getHeaderValue("From").contains("test@bootstrapbaseline.com"));
		assertTrue(message.getHeaderValue("To").contains("user@bootstrapbaseline.com"));
		assertEquals(subject, message.getHeaderValue("Subject"));	
		String body = message.getBody();		
		assertTrue(body.contains("Test Message"));
		assertTrue(body.contains("Default Message"));		
	}
	
	@Test
	public void testSendTemplatedMessageCustomFrom() {

		Map<String, Object> defaultModel = new HashMap<>();
		defaultModel.put("defaultMessage", "Default Message");
		EmailService emailService = newBuilder().addTemplateModel(defaultModel).build();
		
		String subject = "testSendAdminEmail";
		Map<String, Object> messageModel = new HashMap<>();
		messageModel.put("testMessage", "Test Message");		
		emailService.sendTemplatedMessage("customFrom@bootstrapbaseline.com","user@bootstrapbaseline.com", subject, messageModel, "template/test.html");
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertTrue(message.getHeaderValue("From").contains("customFrom@bootstrapbaseline.com"));
		assertTrue(message.getHeaderValue("To").contains("user@bootstrapbaseline.com"));
		assertEquals(subject, message.getHeaderValue("Subject"));	
		String body = message.getBody();		
		assertTrue(body.contains("Test Message"));
		assertTrue(body.contains("Default Message"));		
	}	

	@Test
	public void testOverrideSendAdminEmail() {
		EmailService emailService = newBuilder().adminRecipient("admin@bootstrapbaseline.com").overrideTo(override).build();
		Map<String, Object> adminEmailInfo = new HashMap<>();
		adminEmailInfo.put("Key1", "value1");
		String subject = "testSendAdminEmail";
		emailService.sendAdminEmail(subject, adminEmailInfo);
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertEquals(override, message.getHeaderValue("To"));			
	}
	
	@Test
	public void testOverrideSendMessage() {
		EmailService emailService = newBuilder().overrideTo(override).build();
		String subject = "testSendAdminEmail";
		String mailMessage = "testMessage";
		emailService.sendMessage("user@bootstrapbaseline.com", mailMessage, subject);
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertEquals(subject, message.getHeaderValue("Subject"));
		assertEquals(mailMessage, message.getBody());
		assertEquals(override, message.getHeaderValue("To"));			
	}	
	
	@Test
	public void testOverrideSendMessageMultipleTo() {
		EmailService emailService = newBuilder().overrideTo(override).build();
		String subject = "testSendAdminEmail";
		String mailMessage = "testMessage";
		emailService.sendMessage(new String[]{"user@bootstrapbaseline.com","user2@bootstrapbaseline.com"}, mailMessage, subject);
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		System.out.print(message.getHeaderValue("To"));
		assertEquals(subject, message.getHeaderValue("Subject"));
		assertEquals(mailMessage, message.getBody());
		assertFalse(message.getHeaderValue("To").contains("user@bootstrapbaseline.com"));
		assertFalse(message.getHeaderValue("To").contains("user2@bootstrapbaseline.com"));
		assertEquals(override, message.getHeaderValue("To"));	
		
	}		
	
	@Test
	public void testOverrideSendTemplatedMessageDefaultFrom() {

		Map<String, Object> defaultModel = new HashMap<>();
		defaultModel.put("defaultMessage", "Default Message");
		EmailService emailService = newBuilder().overrideTo(override).addTemplateModel(defaultModel).build();
		
		String subject = "testSendAdminEmail";
		Map<String, Object> messageModel = new HashMap<>();
		messageModel.put("testMessage", "Test Message");		
		emailService.sendTemplatedMessage("user@bootstrapbaseline.com", subject, messageModel, "template/test.html");
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertTrue(message.getHeaderValue("From").contains("test@bootstrapbaseline.com"));
		assertFalse(message.getHeaderValue("To").contains("user@bootstrapbaseline.com"));
		assertEquals(override, message.getHeaderValue("To"));		
		assertEquals(subject, message.getHeaderValue("Subject"));	
		String body = message.getBody();		
		assertTrue(body.contains("Test Message"));
		assertTrue(body.contains("Default Message"));		
	}
	
	@Test
	public void testOverrideSendTemplatedMessageCustomFrom() {

		Map<String, Object> defaultModel = new HashMap<>();
		defaultModel.put("defaultMessage", "Default Message");
		EmailService emailService = newBuilder().overrideTo(override).addTemplateModel(defaultModel).build();
		
		String subject = "testSendAdminEmail";
		Map<String, Object> messageModel = new HashMap<>();
		messageModel.put("testMessage", "Test Message");		
		emailService.sendTemplatedMessage("customFrom@bootstrapbaseline.com","user@bootstrapbaseline.com", subject, messageModel, "template/test.html");
		SmtpMessage message = (SmtpMessage) smtpServer.getReceivedEmail().next();
		assertTrue(message.getHeaderValue("From").contains("customFrom@bootstrapbaseline.com"));
		assertFalse(message.getHeaderValue("To").contains("user@bootstrapbaseline.com"));
		assertEquals(override, message.getHeaderValue("To"));		
		assertEquals(subject, message.getHeaderValue("Subject"));	
		String body = message.getBody();		
		assertTrue(body.contains("Test Message"));
		assertTrue(body.contains("Default Message"));		
	}	

}
