#!/bin/bash

PROFILE=$1

if (( $# != 1 )); then
    echo "Profile was not passed.  Options are local, dev, test, prod"
    exit 1
fi

echo "Creating database for $PROFILE profile."
mvn clean compile -P create-database exec:java -Dspring.profiles.active=$PROFILE,create-database